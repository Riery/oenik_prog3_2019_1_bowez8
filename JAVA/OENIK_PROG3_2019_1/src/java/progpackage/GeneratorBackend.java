/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package progpackage;

import java.util.Random;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author riery
 */
public class GeneratorBackend 
{
    private static final Random _r = new Random();
    
    private static int GenerateRandom(String param) 
    {
        String[] spl = param.split("-");
        int min = Integer.parseInt(spl[0]);
        int max = Integer.parseInt(spl[1]);
        return _r.nextInt((max - min) + 1) + min;
    }
    
    public static String getJson(String DisplayDizeCol, String ProcessorSpeedGhz, String RAMSizeGB, String VideoCardSizeGB) 
    {
        JsonObjectBuilder jobj = Json.createObjectBuilder();
        jobj.add("DAYOFANOUNCEMENT", GenerateRandom(DisplayDizeCol));
        jobj.add("NUMBEROFCPUCORES", GenerateRandom(ProcessorSpeedGhz));
        jobj.add("SIZEOFDISPLAY", GenerateRandom(RAMSizeGB));
        jobj.add("BASEPRICE", GenerateRandom(VideoCardSizeGB));
        JsonObject complete = jobj.build();
        return complete.toString();
    }
}
