/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package progpackage;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author riery
 */
@WebServlet(name = "InputHandler", urlPatterns = {"/InputHandler"})

public class InputHandler extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        PrintWriter out = response.getWriter();
        try {
            String DisplayDizeCol = request.getParameter("DisplayDizeCol");
            if (!DisplayDizeCol.contains("-")) {
                out.println("SYNTAX dayofanouncement: X-Y");
                return;
            }
            String ProcessorSpeedGhz = request.getParameter("ProcessorSpeedGhz");
            if (!ProcessorSpeedGhz.contains("-")) {
                out.println("SYNTAX numberofcpucores: X-Y");
                return;
            }
            String RAMSizeGB = request.getParameter("RAMSizeGB");
            if (!RAMSizeGB.contains("-")) {
                out.println("SYNTAX sizeofdisplay: X-Y");
                return;
            }
            String VideoCardSizeGB = request.getParameter("VideoCardSizeGB");
            if (!VideoCardSizeGB.contains("-")) {
                out.println("SYNTAX baseprice: X-Y");
                return;
            }
            out.println(GeneratorBackend.getJson(DisplayDizeCol, ProcessorSpeedGhz, RAMSizeGB, VideoCardSizeGB));
        }
        catch (Exception e) {
            out.println("error, bad parameters" + e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
