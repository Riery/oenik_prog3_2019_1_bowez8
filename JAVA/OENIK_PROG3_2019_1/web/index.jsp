<%-- 
    Document   : Generator
    Created on : may 10, 2019, 11:10:14 aM
    Author     : riery
--%>

<%@page import="progpackage.GeneratorBackend"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>prog3 2019 1</title>
        <body>
        <%
            if (request.getParameter("DisplayDizeCol") == null
                || request.getParameter("ProcessorSpeedGhz") == null
                || request.getParameter("RAMSizeGB") == null
                || request.getParameter("VideoCardSizeGB") == null) {
                    out.println("parameter error");
            } else {
                ServletContext context = getServletContext();
                RequestDispatcher rd = context.getRequestDispatcher("/InputHandler");
                rd.forward(request, response);
            }
            %>
        </body>
    </head>
</html>
