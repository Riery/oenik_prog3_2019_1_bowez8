﻿// <copyright file="DataBaseHandler.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Controls all of the Data base connections.
    /// </summary>
    public class DataBaseHandler : IDisposable
    {
        private static DataBaseHandler instance;
        private readonly ShopDBEntities shopDBEntities;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataBaseHandler"/> class.
        /// Initializes a new instance of the Database handler class.
        /// </summary>
        public DataBaseHandler()
        {
            this.shopDBEntities = new ShopDBEntities();
        }

        /// <summary>
        /// Gets Instance for singleton use.
        /// </summary>
        public static DataBaseHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DataBaseHandler();
                }

                return instance;
            }
        }

        /// <summary>
        /// Gets Shop data base.
        /// </summary>
        public virtual ShopDBEntities ShopDataBase
        {
            get { return this.shopDBEntities; }
        }

        /// <summary>
        /// Gets all table and select their names.
        /// </summary>
        public virtual List<string> TablesNames
        {
            get
            {
                return this.shopDBEntities.Database.SqlQuery<string>("SELECT name FROM sys.tables ORDER BY name").ToList();
            }
        }

        /// <summary>
        /// Shop database dispose.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// To save the database.
        /// </summary>
        public virtual void SaveDB()
        {
            this.shopDBEntities.SaveChanges();
        }

        /// <summary>
        /// Lists all brand.
        /// </summary>
        /// <returns>List the brands.</returns>
        public virtual List<LaptopBrand> ListBrands()
        {
            return this.shopDBEntities.LaptopBrands.Select(entity => entity).ToList();
        }

        /// <summary>
        /// Lists all laptop.
        /// </summary>
        /// <returns>List the laptops.</returns>
        public virtual List<Laptop> ListLaptops()
        {
            return this.shopDBEntities.Laptops.Select(entity => entity).ToList();
        }

        /// <summary>
        /// Lists all operation systems.
        /// </summary>
        /// <returns>List the operation systems.</returns>
        public virtual List<OperationSystem> ListOperationSystems()
        {
            return this.shopDBEntities.OperationSystems.Select(entity => entity).ToList();
        }

        /// <summary>
        /// Lists all.
        /// </summary>
        /// <returns>Config.</returns>
        public virtual List<Config> ListConfigs()
        {
            return this.shopDBEntities.Configs.Select(entity => entity).ToList();
        }

        /// <summary>
        /// Delete a config.
        /// </summary>
        /// <param name="config">Deleted config.</param>
        public virtual void DeleteConfig(Config config)
        {
            if (this.shopDBEntities.Configs.Any(entity => entity.Id == config.Id))
            {
                this.shopDBEntities.Configs.Remove(config);
            }
        }

        /// <summary>
        /// Delete a laptop.
        /// </summary>
        /// <param name="laptop">Deleted laptop.</param>
        public virtual void DeleteLaptop(Laptop laptop)
        {
            if (this.shopDBEntities.Laptops.Any(entity => entity.Id == laptop.Id))
            {
                this.shopDBEntities.Laptops.Remove(laptop);
            }
        }

        /// <summary>
        /// Delete a brand.
        /// </summary>
        /// <param name="brand">The brand which you want to delete.</param>
        public void DeleteBrand(LaptopBrand brand)
        {
            if (this.shopDBEntities.LaptopBrands.Any(entity => entity.Id == brand.Id))
            {
                this.shopDBEntities.LaptopBrands.Remove(brand);
            }
        }

        /// <summary>
        /// Delete an os.
        /// </summary>
        /// <param name="os">The operation system which you want to delete.</param>
        public void DeleteOS(OperationSystem os)
        {
            if (this.shopDBEntities.OperationSystems.Any(entity => entity.Id == os.Id))
            {
                this.shopDBEntities.OperationSystems.Remove(os);
            }
        }

        /// <summary>
        /// Add a new config.
        /// </summary>
        /// <param name="config">The config which you want to delete.</param>
        public void AddNewConfig(Config config)
        {
            this.shopDBEntities.Configs.Add(config);
        }

        /// <summary>
        /// Add a new brand.
        /// </summary>
        /// <param name="brand">New brand.</param>
        public void AddNewBrand(LaptopBrand brand)
        {
            this.shopDBEntities.LaptopBrands.Add(brand);
        }

        /// <summary>
        /// Add a new laptop.
        /// </summary>
        /// <param name="laptop">new laptop.</param>
        public void AddNewLaptop(Laptop laptop)
        {
            this.shopDBEntities.Laptops.Add(laptop);
        }

        /// <summary>
        /// Add a new os.
        /// </summary>
        /// <param name="os">new os.</param>
        public void AddNewOS(OperationSystem os)
        {
            this.shopDBEntities.OperationSystems.Add(os);
        }

        /// <summary>
        /// Dispose.
        /// </summary>
        /// <param name="disposing">Disposing.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.shopDBEntities != null)
                {
                    this.shopDBEntities.Dispose();
                }
            }
        }
    }
}
