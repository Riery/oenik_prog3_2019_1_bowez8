CREATE TABLE [dbo].[Configs] (
    [Id]                INT           NOT NULL,
    [DisplayType]       NVARCHAR (50) NULL,
    [DisplaySizeCol]    FLOAT (53)    NULL,
    [ProcessorType]     NVARCHAR (50) NULL,
    [ProcessorSpeedGhz] FLOAT (53)    NULL,
    [VideoCardType]     NVARCHAR (50) NULL,
    [VideoCardSizeGB]   INT           NULL,
    [RAMType]           NVARCHAR (50) NULL,
    [RAMSizeGB]         INT           NULL,
    [StorageType]       NVARCHAR (50) NULL,
    [StorageSizeGB]     INT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[LaptopBrands] (
    [Id]      INT            NOT NULL,
    [Name]    NVARCHAR (MAX) NULL,
    [Country] NVARCHAR (MAX) NULL,
    [url]     NVARCHAR (MAX) NULL,
    [CEO]     NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[OperationSystem] (
    [Id]         INT            NOT NULL,
    [Name]       NVARCHAR (MAX) NULL,
    [CEO]        NVARCHAR (MAX) NULL,
    [Foundation] NVARCHAR (MAX) NULL,
    [PriceHUF]   INT            NULL,
    [SizeGB]     INT            NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Laptops] (
    [Id]                 INT NOT NULL,
    [LaptopBrand_Id]     INT NOT NULL,
    [Config_Id]          INT NOT NULL,
    [OperationSystem_Id] INT NOT NULL,
    [PriceHUF]           INT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Laptops_LaptopBrands] FOREIGN KEY ([LaptopBrand_Id]) REFERENCES [dbo].[LaptopBrands] ([Id]),
    CONSTRAINT [FK_Laptops_OperationSystem] FOREIGN KEY ([OperationSystem_Id]) REFERENCES [dbo].[OperationSystem] ([Id]),
    CONSTRAINT [FK_Laptops_Configs] FOREIGN KEY ([Config_Id]) REFERENCES [dbo].[Configs] ([Id])
);