﻿// <copyright file="IEntity.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Interfaces
{
    /// <summary>
    /// Gets Generic classes.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Gets or sets identifier.
        /// </summary>
        int Id { get; set; }
    }
}
