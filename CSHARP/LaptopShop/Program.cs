﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LaptopShop
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Returns with the Java url.
        /// </summary>
        public const string Javaurl = "http://localhost:8080/OENIK_PROG3_2019_1/";

        /// <summary>
        /// Main.
        /// </summary>
        public static void Main()
        {
            Menu.DisplayMenu();
        }
    }
}
