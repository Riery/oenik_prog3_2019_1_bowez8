﻿// <copyright file="ConfigJSON.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LaptopShop
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for the JSON data.
    /// </summary>
    public class ConfigJson
    {
        /// <summary>
        /// Gets or sets the display size in col.
        /// </summary>
        public string DisplaySizeCol
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the processor speed in Ghz.
        /// </summary>
        public string ProcessorSpeedGhz
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ram size in GB.
        /// </summary>
        public string RamSizeGB
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the video card size in GB.
        /// </summary>
        public string VideoCardSizeGB
        {
            get;
            set;
        }
    }
}
