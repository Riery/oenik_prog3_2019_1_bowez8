﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LaptopShop
{
    using System;
    using System.Net;
    using System.Text;
    using Data;
    using Logic;
    using Newtonsoft.Json;

    /// <summary>
    /// Menu class.
    /// </summary>
    public static class Menu
    {
        /// <summary>
        /// Displays menu.
        /// </summary>
        public static void DisplayMenu()
        {
            ConsoleKeyInfo cki;

            do
            {
                Console.Clear();
                Console.WriteLine("LaptopShop");
                Console.WriteLine();
                Console.WriteLine("1-> Laptops");
                Console.WriteLine("2-> Brands");
                Console.WriteLine("3-> Configs");
                Console.WriteLine("4-> OperationSystems");
                Console.WriteLine("5-> Java");
                Console.WriteLine("Esc-> Exit");

                cki = Console.ReadKey(false);
                switch (cki.KeyChar.ToString())
                {
                    case "1":
                        LaptopMenu();
                        break;
                    case "2":
                        BrandMenu();
                        break;
                    case "3":
                        ConfigMenu();
                        break;
                    case "4":
                        OSMenu();
                        break;
                    case "5":
                        JavaMenu();
                        break;
                }

                Console.Clear();
            }
            while (cki.Key != ConsoleKey.Escape);
        }

        /// <summary>
        /// Laptop menu.
        /// </summary>
        public static void LaptopMenu()
        {
            Console.Clear();
            ConsoleKeyInfo cki;

            CommonLogic.Instance.SubMenu("Laptop", "Laptops below 200K Huf");

            cki = Console.ReadKey(false);
            switch (cki.KeyChar.ToString())
            {
                case "1":
                    CommonLogic.Instance.ConsoleWrite(LaptopLogic.Instance.ListAll());
                    Console.WriteLine("\nPress any button to back to main menu.");
                    Console.ReadKey();
                    break;
                case "2":
                    CommonLogic.Instance.AddLaptop();
                    break;
                case "3":
                    CommonLogic.Instance.RemoveLaptop();
                    break;
                case "4":
                    CommonLogic.Instance.UpdateLaptop();
                    break;
                case "\b":
                    DisplayMenu();
                    break;
            }

            Console.Clear();
        }

        /// <summary>
        /// Brand menu.
        /// </summary>
        public static void BrandMenu()
        {
            Console.Clear();
            ConsoleKeyInfo cki;

            CommonLogic.Instance.SubMenu("Brand", "-");

            cki = Console.ReadKey(false);
            switch (cki.KeyChar.ToString())
            {
                case "1":
                    CommonLogic.Instance.ConsoleWrite(BrandLogic.Instance.ListAll());
                    Console.WriteLine("\nPress any button to back to main menu.");
                    Console.ReadKey();
                    break;
                case "2":
                    CommonLogic.Instance.AddBrand();
                    break;
                case "3":
                    CommonLogic.Instance.RemoveBrand();
                    break;
                case "4":
                    CommonLogic.Instance.UpdateBrand();
                    break;
                case "\b":
                    DisplayMenu();
                    break;
            }

            Console.Clear();
        }

        /// <summary>
        /// Config menu.
        /// </summary>
        public static void ConfigMenu()
        {
            Console.Clear();
            ConsoleKeyInfo cki;

            CommonLogic.Instance.SubMenu("Config", "i7 and at least 16GB Ram");

            cki = Console.ReadKey(false);
            switch (cki.KeyChar.ToString())
            {
                case "1":
                    CommonLogic.Instance.ConsoleWrite(ConfigLogic.Instance.ListAll());
                    Console.WriteLine("\nPress any button to back to main menu.");
                    Console.ReadKey();
                    break;
                case "2":
                    CommonLogic.Instance.AddConfig();
                    break;
                case "3":
                    CommonLogic.Instance.RemoveConfig();
                    break;
                case "4":
                    CommonLogic.Instance.UpdateConfig();
                    break;
                case "\b":
                    DisplayMenu();
                    break;
            }

            Console.Clear();
        }

        /// <summary>
        /// Operation system menu.
        /// </summary>
        public static void OSMenu()
        {
            Console.Clear();
            ConsoleKeyInfo cki;

            CommonLogic.Instance.SubMenu("Operation system", "List Free operation systems");

            cki = Console.ReadKey(false);
            switch (cki.KeyChar.ToString())
            {
                case "1":
                    CommonLogic.Instance.ConsoleWrite(OSLogic.Instance.ListAll());
                    Console.WriteLine("\nPress any button to back to main menu.");
                    Console.ReadKey();
                    break;
                case "2":
                    CommonLogic.Instance.AddOS();
                    break;
                case "3":
                    CommonLogic.Instance.RemoveOS();
                    break;
                case "4":
                    CommonLogic.Instance.UpdateOS();
                    break;
                case "5":
                    OSLogic.Instance.ListAllFreeOS();
                    break;
                case "\b":
                    DisplayMenu();
                    break;
            }

            Console.Clear();
        }

        private static void JavaMenu()
        {
            Console.WriteLine("Írj be egy minimum Kijelző méretet colban!");
            string dsizemin = Console.ReadLine();

            Console.WriteLine("Írj be egy maximum Kijelző méretet colban!");
            string dsizemax = Console.ReadLine();

            Console.WriteLine("Írj be egy minimum processzor sebességet Giga hertzben!");
            string pspeedmin = Console.ReadLine();

            Console.WriteLine("Írj be egy maximum processzor sebességet Giga hertzben!");
            string pspeedmax = Console.ReadLine();

            Console.WriteLine("Írj be egy minimum ram méretet gigabájtban!");
            string rsizemin = Console.ReadLine();

            Console.WriteLine("Írj be egy maximum ram méretet gigabájtban!");
            string rsizemax = Console.ReadLine();

            Console.WriteLine("Írj be egy minimum videó kártya méretet gigabájtban!");
            string vcsizemin = Console.ReadLine();

            Console.WriteLine("Írj be egy maximum videó kártya méretet gigabájtban!");
            string vcsizemax = Console.ReadLine();

            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                string fullresponse = client.DownloadString(Program.Javaurl +
                                                              "?displaySizeCol=" + dsizemin + "-" + dsizemax +
                                                              "&processorSpeedGhz=" + pspeedmin + "-" + pspeedmax +
                                                              "&ramSizeGB=" + rsizemin + "-" + rsizemax +
                                                              "&videoCardSizeGB=" + vcsizemin + "-" + vcsizemax);

                if (fullresponse.Contains("Hiba!") || fullresponse.Contains("Hibás") || fullresponse.Contains("SYNTAX"))
                {
                    Console.WriteLine("HIBA: " + fullresponse);
                }
                else
                {
                    Console.WriteLine("Válasz: " + fullresponse);

                    ConfigJson cfg = JsonConvert.DeserializeObject<ConfigJson>(fullresponse);

                    int displaySizeCol = int.Parse(cfg.DisplaySizeCol);
                    int processorSpeedGhz = int.Parse(cfg.ProcessorSpeedGhz);
                    int ramSizeGB = int.Parse(cfg.RamSizeGB);
                    int videoCardSizeGB = int.Parse(cfg.VideoCardSizeGB);

                    Console.WriteLine("Írd be a config id-jét!");
                    string id = Console.ReadLine();
                    int idy;
                    bool b2 = int.TryParse(id, out idy);
                    while (!b2)
                    {
                        Console.WriteLine("Írd be a config id-jét!");
                        id = Console.ReadLine();
                        b2 = int.TryParse(id, out idy);
                    }

                    var config = new Config()
                    {
                        Id = idy,
                        DisplaySizeCol = displaySizeCol,
                        ProcessorSpeedGhz = processorSpeedGhz,
                        RAMSizeGB = ramSizeGB,
                        VideoCardSizeGB = videoCardSizeGB,
                    };

                    bool success = ConfigLogic.Instance.Insert(config);
                    string text = success ? "Success" : "Error";
                    Console.WriteLine(text);
                }
            }
        }
    }
}
