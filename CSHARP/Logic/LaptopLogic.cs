﻿// <copyright file="LaptopLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data;
    using Data.Repos;

    /// <summary>
    /// Contains the logics which belongs to Laptops.
    /// </summary>
    public class LaptopLogic
    {
        private static LaptopLogic laptoplogic;
        private BaseRepository baseRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LaptopLogic"/> class.
        /// Constructor for laptop logic.
        /// </summary>
        public LaptopLogic()
        {
            this.baseRepo = new BaseRepository();
        }

        /// <summary>
        /// Gets laptop logic for singleton use.
        /// </summary>
        public static LaptopLogic Instance
        {
            get
            {
                if (laptoplogic == null)
                {
                    laptoplogic = new LaptopLogic();
                }

                return laptoplogic;
            }
        }

        /// <summary>
        /// List all Laptop from repository.
        /// </summary>
        /// <returns>Returns with a string builder.</returns>
        public StringBuilder ListAll()
        {
            return this.baseRepo.LaptopRepository.ListTableContents();
        }

        /// <summary>
        /// List all Laptop from repository.
        /// </summary>
        /// <returns>Returns with a string builder.</returns>
        public virtual List<Laptop> ListAllAsLaptop()
        {
            return this.baseRepo.LaptopRepository.ListAll().ToList();
        }

        /// <summary>
        /// Sends the selected laptop to the laptop repository for remove.
        /// </summary>
        /// <param name="id">Remove laptop Id.</param>
        /// <returns>Success or not.</returns>
        public bool Remove(int id)
        {
            return this.baseRepo.LaptopRepository.Delete(this.baseRepo.LaptopRepository.ListAll().FirstOrDefault(b => b.Id == id).Id);
        }

        /// <summary>
        /// Sends the laptop to the laptop repository for insert.
        /// </summary>
        /// <param name="temp">Inserted laptop.</param>
        /// <returns>Succes or not.</returns>
        public virtual bool Insert(Laptop temp)
        {
            return this.baseRepo.LaptopRepository.Insert(temp.LaptopBrand, temp.Config, temp.OperationSystem, temp.PriceHuf);
        }

        /// <summary>
        /// Sends the selected laptop to the laptop repository for update.
        /// </summary>
        /// <param name="laptop">Updated laptop.</param>
        /// <returns>Succes or not.</returns>
        public bool Update(Laptop laptop)
        {
            return this.baseRepo.LaptopRepository.Update(laptop.Id, laptop.LaptopBrand, laptop.Config, laptop.OperationSystem, laptop.PriceHuf);
        }

        /// <summary>
        /// Get laptop by its id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Laptop.</returns>
        public Laptop GetLaptopById(int id)
        {
            return this.baseRepo.LaptopRepository.ListAll().FirstOrDefault(entity => entity.Id == id);
        }

        /// <summary>
        /// Is laptop exsists.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Exsists or not.</returns>
        public bool CheckIsLaptopExsists(int id)
        {
           return this.baseRepo.LaptopRepository.CheckIfLaptopExists(id);
        }
    }
}
