﻿// <copyright file="OSLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data;
    using Data.Repos;

    /// <summary>
    /// Contains the logics which belongs to OperationSystems.
    /// </summary>
    public class OSLogic
    {
        private static OSLogic oslogic;
        private BaseRepository baseRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="OSLogic"/> class.
        /// Operation system logic constructor.
        /// </summary>
        public OSLogic()
        {
            this.baseRepo = new BaseRepository();
        }

        /// <summary>
        /// Gets For singleton use of the operation system logic.
        /// </summary>
        public static OSLogic Instance
        {
            get
            {
                if (oslogic == null)
                {
                    oslogic = new OSLogic();
                }

                return oslogic;
            }
        }

        /// <summary>
        /// Gets base repository getter.
        /// </summary>
        public virtual BaseRepository BaseRepository
        {
            get
            {
                return this.baseRepo;
            }
        }

        /// <summary>
        /// List all OS from repository.
        /// </summary>
        /// <returns>Returns with a string builder.</returns>
        public StringBuilder ListAll()
        {
            return this.baseRepo.OSRepository.ListTableContents();
        }

        /// <summary>
        /// List all OS from repository.
        /// </summary>
        /// <returns>Returns with a string builder.</returns>
        public virtual List<OperationSystem> ListAllAsOS()
        {
            return this.baseRepo.OSRepository.ListAll().ToList();
        }

        /// <summary>
        /// Sends the os to the os repository for insert.
        /// </summary>
        /// <param name="os">The inserted operation system.</param>
        /// <returns>Succes or not.</returns>
        public virtual bool Insert(OperationSystem os)
        {
            return this.baseRepo.OSRepository.Insert(os.Name, os.Ceo, os.Foundation, (int)os.SizeGB, (int)os.PriceHuf);
        }

        /// <summary>
        /// Update an operation system in the base repository.
        /// </summary>
        /// <param name="os">Operation system.</param>
        /// <returns>Succes or not.</returns>
        public bool Update(OperationSystem os)
        {
            return this.baseRepo.OSRepository.Update(os.Id, os.Name, os.Ceo, os.Foundation, (int)os.SizeGB, (int)os.PriceHuf);
        }

        /// <summary>
        /// Sends the selected os to the operation system repository for remove.
        /// </summary>
        /// <param name="id">The id of the operation system which the user wants to remove.</param>
        /// <returns>Success or not.</returns>
        public bool Remove(int id)
        {
            return this.baseRepo.OSRepository.Delete(this.baseRepo.OSRepository.ListAll().FirstOrDefault(b => b.Id == id).Id);
        }

        /// <summary>
        /// Get an os by id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Operation system.</returns>
        public OperationSystem GetOSById(int id)
        {
            return this.baseRepo.OSRepository.ListAll().FirstOrDefault(entity => entity.Id == id);
        }

        /// <summary>
        /// Lists all free operation system.
        /// </summary>
        public void ListAllFreeOS()
        {
            CommonLogic.Instance.ConsoleWrite(this.baseRepo.OSRepository.ListAllFreeOS());
        }

        /// <summary>
        /// Is OS exsists.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Exsists or not.</returns>
        public bool CheckIsOSExsists(int id)
        {
            return this.baseRepo.OSRepository.CheckIfOSExists(id);
        }
    }
}
