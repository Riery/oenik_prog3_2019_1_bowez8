﻿// <copyright file="BrandLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data;
    using Data.Repos;

    /// <summary>
    /// Contains the logics which belongs to Brands.
    /// </summary>
    public class BrandLogic
    {
        private static BrandLogic brandlogic;
        private BaseRepository baseRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrandLogic"/> class.
        /// Ctor.
        /// </summary>
        public BrandLogic()
        {
            this.baseRepo = new BaseRepository();
        }

        /// <summary>
        /// Gets for singleton use.
        /// </summary>
        public static BrandLogic Instance
        {
            get
            {
                if (brandlogic == null)
                {
                    brandlogic = new BrandLogic();
                }

                return brandlogic;
            }
        }

        /// <summary>
        /// Gets base repository getter.
        /// </summary>
        public virtual BaseRepository BaseRepository
        {
            get
            {
                return this.baseRepo;
            }
        }

        /// <summary>
        /// List all Brand from repository.
        /// </summary>
        /// <returns>Returns a string list.</returns>
        public StringBuilder ListAll()
        {
            return this.baseRepo.BrandRepository.ListTableContents();
        }

        /// <summary>
        /// List all.
        /// </summary>
        /// <returns>List.</returns>
        public virtual List<LaptopBrand> ListAllAsBrand()
        {
            return this.baseRepo.BrandRepository.ListAll().ToList();
        }

        /// <summary>
        /// Sends the brand to the brand repository for insert.
        /// </summary>
        /// <param name="b">The brand which the user want to insert.</param>
        /// <returns>Success or not.</returns>
        public virtual bool Insert(LaptopBrand b)
        {
            return this.baseRepo.BrandRepository.Insert(b.Ceo, b.Country, b.Name, b.Url);
        }

        /// <summary>
        /// Sends the selected brand to the brand repository for update.
        /// </summary>
        /// <param name="b">The brand which the user want to Update.</param>
        /// <returns>Success or not.</returns>
        public bool Update(LaptopBrand b)
        {
            return this.baseRepo.BrandRepository.Update(b.Id, b.Ceo, b.Country, b.Name, b.Url);
        }

        /// <summary>
        /// Sends the selected brand to the brand repository for remove.
        /// </summary>
        /// <param name="id">The id of the brand which the user wants to remove.</param>
        /// <returns>Success or not.</returns>
        public bool Remove(int id)
        {
           return this.baseRepo.BrandRepository.Delete(this.baseRepo.BrandRepository.ListAll().FirstOrDefault(b => b.Id == id).Id);
        }

        /// <summary>
        /// Get a brand by its id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Laptop brand.</returns>
        public LaptopBrand GetBrandById(int id)
        {
            return this.baseRepo.BrandRepository.ListAll().FirstOrDefault(entity => entity.Id == id);
        }

        /// <summary>
        /// Is Brand exsists.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Exsists or not.</returns>
        public bool CheckIsBrandExsists(int id)
        {
            return this.baseRepo.BrandRepository.CheckIfBrandExists(id);
        }
    }
}
