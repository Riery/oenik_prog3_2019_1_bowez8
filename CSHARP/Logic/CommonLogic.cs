﻿// <copyright file="CommonLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Text;
    using Data;

    /// <summary>
    /// Connects the menu with the logic layer.
    /// </summary>
    public class CommonLogic
    {
        private static CommonLogic commonLogic;

        /// <summary>
        /// Gets for singleton use.
        /// </summary>
        public static CommonLogic Instance
        {
            get
            {
                if (commonLogic == null)
                {
                    commonLogic = new CommonLogic();
                }

                return commonLogic;
            }
        }

        /// <summary>
        /// Writes a list to console.
        /// </summary>
        /// <param name="list">List.</param>
        public void ConsoleWrite(StringBuilder list)
        {
            Console.Clear();
            Console.WriteLine(list.ToString());
        }

        /// <summary>
        /// Create sub menus in console.
        /// </summary>
        /// <param name="menu">Menu.</param>
        /// <param name="extra">Extra.</param>
        public void SubMenu(string menu, string extra)
        {
            Console.WriteLine(menu + "s");
            Console.WriteLine();
            Console.WriteLine("1-> List " + menu);
            Console.WriteLine("2-> Add a " + menu);
            Console.WriteLine("3-> Delete a " + menu);
            Console.WriteLine("4-> Update a " + menu);
            Console.WriteLine("5-> " + extra);
            Console.WriteLine("Backspace-> Back");
        }

        /// <summary>
        /// Create a brand and adds it to brand logic.
        /// </summary>
        public void AddBrand()
        {
            LaptopBrand temp = new LaptopBrand();

            Console.Clear();
            Console.WriteLine("Add Brand name:");
            temp.Name = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Add country:");
            temp.Country = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Add url:");
            temp.Url = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Add ceo:");
            temp.Ceo = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Brand added !");

            BrandLogic.Instance.Insert(temp);
        }

        /// <summary>
        /// Create a config and adds it to config logic.
        /// </summary>
        public void AddConfig()
        {
            Config temp = new Config();

            Console.Clear();
            Console.WriteLine("Add Display Type:");
            temp.DisplayType = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Add Display Size:");
            temp.DisplaySizeCol = double.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Add Processor Type:");
            temp.ProcessorType = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Add Processor Speed (Ghz):");
            temp.ProcessorSpeedGhz = double.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Add RAM Type:");
            temp.RAMType = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Add RAM Size (GB):");
            temp.RAMSizeGB = int.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Add VideoCard Type:");
            temp.VideoCardType = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Add VideoCard Size (GB):");
            temp.VideoCardSizeGB = int.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Add Storage Type:");
            temp.StorageType = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Add Storage Size (GB):");
            temp.StorageSizeGB = int.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Config Added!");

            ConfigLogic.Instance.Insert(temp);
        }

        /// <summary>
        /// Create an os and adds to os logic.
        /// </summary>
        public void AddOS()
        {
            OperationSystem temp = new OperationSystem();

            Console.Clear();
            Console.WriteLine("Add Name:");
            temp.Name = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Add CEO:");
            temp.Ceo = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Add Foundation:");
            temp.Foundation = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Add SizeGB:");
            temp.SizeGB = int.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Add PriceHUF:");
            temp.PriceHuf = int.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("OS Added!");

            OSLogic.Instance.Insert(temp);
        }

        /// <summary>
        /// Create a laptop and adds to laptop logic.
        /// </summary>
        public void AddLaptop()
        {
            Laptop temp = new Laptop();
            Console.Clear();
            this.ConsoleWrite(BrandLogic.Instance.ListAll());
            Console.WriteLine("Select a Brand : (type a number)");
            var brandId = Console.ReadLine();
            temp.LaptopBrand = BrandLogic.Instance.GetBrandById(int.Parse(brandId));
            Console.Clear();

            this.ConsoleWrite(ConfigLogic.Instance.ListAll());
            Console.WriteLine("Select a Config : (type a number)");
            var configId = Console.ReadLine();
            temp.Config = ConfigLogic.Instance.GetConfigById(int.Parse(configId));
            Console.Clear();

            this.ConsoleWrite(OSLogic.Instance.ListAll());
            Console.WriteLine("Select a OperationSystem : (type a number)");
            var osId = Console.ReadLine();
            temp.OperationSystem = OSLogic.Instance.GetOSById(int.Parse(osId));
            Console.Clear();

            Console.WriteLine("Add a price in HUF:");
            temp.PriceHuf = int.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Laptop Added!");

            LaptopLogic.Instance.Insert(temp);
        }

        /// <summary>
        /// Selects a brand witch the user want to delete and send its id to brand logic.
        /// </summary>
        public void RemoveBrand()
        {
            this.ConsoleWrite(BrandLogic.Instance.ListAll());
            Console.WriteLine("Which one you want to delete ? (type a number)");
            var id = int.Parse(Console.ReadLine());
            BrandLogic.Instance.Remove(id);
        }

        /// <summary>
        /// Selects an os witch the user want to delete and send its id to os logic.
        /// </summary>
        public void RemoveOS()
        {
            this.ConsoleWrite(OSLogic.Instance.ListAll());
            Console.WriteLine(" - Which one you want to delete ? (type a number)");
            var id = int.Parse(Console.ReadLine());
            OSLogic.Instance.Remove(id);
        }

        /// <summary>
        /// Selects a config witch the user want to delete and send its id to config logic.
        /// </summary>
        public void RemoveConfig()
        {
            this.ConsoleWrite(ConfigLogic.Instance.ListAll());
            Console.WriteLine("Which one you want to delete ? (type a number)");
            var id = GetId();
            ConfigLogic.Instance.Remove(id);
        }

        /// <summary>
        /// Selects a laptop witch the user want to delete and send its id to laptop logic.
        /// </summary>
        public void RemoveLaptop()
        {
            this.ConsoleWrite(LaptopLogic.Instance.ListAll());
            Console.WriteLine("Which one you want to delete ? (type a number)");
            var id = int.Parse(Console.ReadLine());
            LaptopLogic.Instance.Remove(id);
        }

        /// <summary>
        /// Update a brand and send it to brand logic.
        /// </summary>
        public void UpdateBrand()
        {
            Console.Clear();
            this.ConsoleWrite(BrandLogic.Instance.ListAll());
            Console.WriteLine("Select a brand by tiping a number");
            var brandId = int.Parse(Console.ReadLine());

            LaptopBrand brand = BrandLogic.Instance.GetBrandById(brandId);
            if (brand != null)
            {
                Console.Clear();
                Console.WriteLine("You want to change the name ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add Brand name:");
                    brand.Name = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("You want to change the country ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add Brand country:");
                    brand.Country = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("You want to change the url ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add brand url:");
                    brand.Url = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("You want to change the CEO ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add brand CEO:");
                    brand.Ceo = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("Brand updated !");

                BrandLogic.Instance.Update(brand);
            }
        }

        /// <summary>
        /// Update an os and send it to os logic.
        /// </summary>
        public void UpdateOS()
        {
            Console.Clear();
            this.ConsoleWrite(OSLogic.Instance.ListAll());
            Console.WriteLine("Select an os by tiping a number");
            var osId = int.Parse(Console.ReadLine());

            OperationSystem os = OSLogic.Instance.GetOSById(osId);
            if (os != null)
            {
                Console.Clear();
                Console.WriteLine("You want to change the name ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add os name:");
                    os.Name = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("You want to change the CEO ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add CEO:");
                    os.Ceo = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("You want to change the Foundation ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add Foundation:");
                    os.Foundation = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("You want to change the SizeGB ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add SizeGB:");
                    os.SizeGB = int.Parse(Console.ReadLine());
                }

                Console.Clear();

                Console.WriteLine("You want to change the PriceHUF ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add PriceHUF:");
                    os.PriceHuf = int.Parse(Console.ReadLine());
                }

                Console.Clear();

                Console.WriteLine("Operation system updated !");

                OSLogic.Instance.Update(os);
            }
        }

        /// <summary>
        /// Update a config and send it to config logic.
        /// </summary>
        public void UpdateConfig()
        {
            Console.Clear();
            this.ConsoleWrite(ConfigLogic.Instance.ListAll());
            Console.WriteLine("Select a config by tiping a number");
            var configId = int.Parse(Console.ReadLine());

            Config c = ConfigLogic.Instance.GetConfigById(configId);
            if (c != null)
            {
                Console.Clear();
                Console.WriteLine("You want to change the Display Type ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add Display Type:");
                    c.DisplayType = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("You want to change the Display Size ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add Display Size:");
                    c.DisplaySizeCol = double.Parse(Console.ReadLine());
                }

                Console.Clear();

                Console.WriteLine("You want to change the Processor Type ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add Processor Type:");
                    c.ProcessorType = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("You want to change the Processor Speed ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add Processor Speed (Ghz):");
                    c.ProcessorSpeedGhz = double.Parse(Console.ReadLine());
                }

                Console.Clear();

                Console.WriteLine("You want to change the RAM Type ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add RAM Type:");
                    c.RAMType = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("You want to change the RAM Size ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add RAM Size (GB):");
                    c.RAMSizeGB = int.Parse(Console.ReadLine());
                }

                Console.Clear();

                Console.WriteLine("You want to change the VideoCard Type ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add VideoCard Type:");
                    c.VideoCardType = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("You want to change the VideoCard Size ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add VideoCard Size (GB):");
                    c.VideoCardSizeGB = int.Parse(Console.ReadLine());
                }

                Console.Clear();

                Console.WriteLine("You want to change the Storage Type ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add Storage Type:");
                    c.StorageType = Console.ReadLine();
                }

                Console.Clear();

                Console.WriteLine("You want to change the Storage Size ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add Storage Size (GB):");
                    c.StorageSizeGB = int.Parse(Console.ReadLine());
                }

                Console.Clear();

                Console.WriteLine("Config updated !");

                ConfigLogic.Instance.Update(c);
            }
        }

        /// <summary>
        /// Update a laptop and send it to laptop logic.
        /// </summary>
        public void UpdateLaptop()
        {
            Console.Clear();
            this.ConsoleWrite(LaptopLogic.Instance.ListAll());
            Console.WriteLine("Select a laptop by tiping a number");
            var laptopId = int.Parse(Console.ReadLine());

            Laptop laptop = LaptopLogic.Instance.GetLaptopById(laptopId);
            if (laptop != null)
            {
                Console.Clear();
                Console.WriteLine("You want to change the name ? (y - Yes || n - No)");
                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    this.ConsoleWrite(BrandLogic.Instance.ListAll());
                    Console.WriteLine("Select a Brand : (type a number)");
                    var brandId = Console.ReadLine();
                    laptop.LaptopBrand = BrandLogic.Instance.GetBrandById(int.Parse(brandId));
                    Console.Clear();
                }

                Console.Clear();

                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    this.ConsoleWrite(ConfigLogic.Instance.ListAll());
                    Console.WriteLine("Select a Config : (type a number)");
                    var configId = Console.ReadLine();
                    laptop.Config = ConfigLogic.Instance.GetConfigById(int.Parse(configId));
                }

                Console.Clear();

                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    this.ConsoleWrite(OSLogic.Instance.ListAll());
                    Console.WriteLine("Select a OperationSystem : (type a number)");
                    var osId = Console.ReadLine();
                    laptop.OperationSystem = OSLogic.Instance.GetOSById(int.Parse(osId));
                }

                Console.Clear();

                if (Console.ReadKey().KeyChar == 'y')
                {
                    Console.Clear();
                    Console.WriteLine("Add a price in HUF:");
                    laptop.PriceHuf = int.Parse(Console.ReadLine());
                }

                Console.Clear();

                Console.WriteLine("Laptop updated !");

                LaptopLogic.Instance.Update(laptop);
            }
        }

        private static int GetId()
        {
            return int.Parse(Console.ReadLine());
        }
    }
}
