﻿// <copyright file="ConfigLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data;
    using Data.Repos;

    /// <summary>
    /// Contains the logics which belongs to Configs.
    /// </summary>
    public class ConfigLogic
    {
        private static ConfigLogic configlogic;
        private BaseRepository baseRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigLogic"/> class.
        /// Constructor.
        /// </summary>
        public ConfigLogic()
        {
            this.baseRepo = new BaseRepository();
        }

        /// <summary>
        /// Gets for singleton use of the Config logic.
        /// </summary>
        public static ConfigLogic Instance
        {
            get
            {
                if (configlogic == null)
                {
                    configlogic = new ConfigLogic();
                }

                return configlogic;
            }
        }

        /// <summary>
        /// Gets base repository getter.
        /// </summary>
        public virtual BaseRepository BaseRepository
        {
            get
            {
                return this.baseRepo;
            }
        }

        /// <summary>
        /// List all config from repository.
        /// </summary>
        /// <returns>Returns with a string builder list.</returns>
        public StringBuilder ListAll()
        {
            return this.baseRepo.ConfigRepository.ListTableContents();
        }

        /// <summary>
        /// List all config from repository.
        /// </summary>
        /// <returns>Returns with a string builder list.</returns>
        public virtual List<Config> ListAllAsConfig()
        {
            return this.baseRepo.ConfigRepository.ListAll().ToList();
        }

        /// <summary>
        /// Sends the config to the config repository for insert.
        /// </summary>
        /// <param name="config">The config which the user want to insert.</param>
        /// <returns>Success or not.</returns>
        public virtual bool Insert(Config config)
        {
            return this.baseRepo.ConfigRepository.Insert(config.DisplayType, (double)config.DisplaySizeCol, config.ProcessorType, (double)config.ProcessorSpeedGhz, config.RAMType, (int)config.RAMSizeGB, config.VideoCardType, (int)config.VideoCardSizeGB, config.StorageType, (int)config.StorageSizeGB);
        }

        /// <summary>
        /// Sends the selected config to the config repository for update.
        /// </summary>
        /// <param name="c">Config.</param>
        /// <returns>Success or not.</returns>
        public bool Update(Config c)
        {
            return this.baseRepo.ConfigRepository.Update(c.Id, c.DisplayType, (double)c.DisplaySizeCol, c.ProcessorType, (double)c.ProcessorSpeedGhz, c.RAMType, (int)c.RAMSizeGB, c.VideoCardType, (int)c.VideoCardSizeGB, c.StorageType, (int)c.StorageSizeGB);
        }

        /// <summary>
        /// Sends the selected config to the config repository for remove.
        /// </summary>
        /// <param name="id">The id of the config which the user wants to remove.</param>
        public void Remove(int id)
        {
            this.baseRepo.ConfigRepository.Delete(this.baseRepo.ConfigRepository.ListAll().FirstOrDefault(b => b.Id == id).Id);
        }

        /// <summary>
        /// Get a config by its id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Config.</returns>
        public Config GetConfigById(int id)
        {
            return this.baseRepo.ConfigRepository.ListAll().FirstOrDefault(entity => entity.Id == id);
        }

        /// <summary>
        /// Is Config exsists.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Exsists or not.</returns>
        public bool CheckIsConfigExsists(int id)
        {
            return this.baseRepo.ConfigRepository.CheckIfConfigExists(id);
        }
    }
}
