﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Logic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        /// <summary>
        /// Simple insert check.
        /// </summary>
        [Test]
        public void SingleInsertBrandCheck()
        {
            BrandLogic m = Mock.Of<BrandLogic>();
            Mock<BrandLogic> m2 = Mock.Get(m);
            List<LaptopBrand> lista = new List<LaptopBrand>() { new LaptopBrand() { Id = 666666, Name = "Asus", Country = "Tajvan", Ceo = "Elemér", Url = "ezegyweboldal.com" } };

            m2.Setup(x => x.ListAllAsBrand()).Returns(lista);
            m2.Object.Insert(new LaptopBrand() { Id = 666666, Name = "Asus", Country = "Tajvan", Ceo = "Elemér", Url = "ezegyweboldal.com" });
            m2.Verify(mock => mock.Insert(new LaptopBrand() { Id = 666666, Name = "Asus", Country = "Tajvan", Ceo = "Elemér", Url = "ezegyweboldal.com" }), Times.Never());
        }

        /// <summary>
        /// Simple insert check.
        /// </summary>
        [Test]
        public void SingleInsertOperationSystemCheck()
        {
            OSLogic m = Mock.Of<OSLogic>();
            Mock<OSLogic> m2 = Mock.Get(m);
            List<OperationSystem> lista = new List<OperationSystem>() { new OperationSystem() { Id = 666666, Name = "Windows", Foundation = "2014", Ceo = "Elemér", SizeGB = 15, PriceHuf = 500 } };

            m2.Setup(x => x.ListAllAsOS()).Returns(lista);
            m2.Object.Insert(new OperationSystem() { Id = 666666, Name = "Windows", Foundation = "2014", Ceo = "Elemér", SizeGB = 15, PriceHuf = 500 });
            m2.Verify(mock => mock.Insert(new OperationSystem() { Id = 666666, Name = "Windows", Foundation = "2014", Ceo = "Elemér", SizeGB = 15, PriceHuf = 500 }), Times.Never());
        }

        /// <summary>
        /// Simple insert check.
        /// </summary>
        [Test]
        public void SingleInsertConfigCheck()
        {
            ConfigLogic m = Mock.Of<ConfigLogic>();
            Mock<ConfigLogic> m2 = Mock.Get(m);
            List<Config> lista = new List<Config>() { new Config() { Id = 666666, DisplaySizeCol = 15.6, DisplayType = "FullHD", ProcessorType = "I9-9900K", ProcessorSpeedGhz = 5, RAMSizeGB = 128, RAMType = "DDR5", VideoCardType = "GTX 3080TI", VideoCardSizeGB = 24, StorageType = "SSD", StorageSizeGB = 2000 } };

            m2.Setup(x => x.ListAllAsConfig()).Returns(lista);
            m2.Object.Insert(new Config() { Id = 666666, DisplaySizeCol = 15.6, DisplayType = "FullHD", ProcessorType = "I9-9900K", ProcessorSpeedGhz = 5, RAMSizeGB = 128, RAMType = "DDR5", VideoCardType = "GTX 3080TI", VideoCardSizeGB = 24, StorageType = "SSD", StorageSizeGB = 2000 });
            m2.Verify(mock => mock.Insert(new Config() { Id = 666666, DisplaySizeCol = 15.6, DisplayType = "FullHD", ProcessorType = "I9-9900K", ProcessorSpeedGhz = 5, RAMSizeGB = 128, RAMType = "DDR5", VideoCardType = "GTX 3080TI", VideoCardSizeGB = 24, StorageType = "SSD", StorageSizeGB = 2000 }), Times.Never());
        }

        /// <summary>
        /// Simple insert check.
        /// </summary>
        [Test]
        public void SingleInsertLaptopCheck()
        {
            LaptopLogic m = Mock.Of<LaptopLogic>();
            Mock<LaptopLogic> m2 = Mock.Get(m);
            List<Laptop> lista = new List<Laptop>() { new Laptop() { Id = 666666, LaptopBrand = new LaptopBrand() { Id = 666666, Name = "Asus", Country = "Tajvan", Ceo = "Elemér", Url = "ezegyweboldal.com" }, Config = new Config() { Id = 666666, DisplaySizeCol = 15.6, DisplayType = "FullHD", ProcessorType = "I9-9900K", ProcessorSpeedGhz = 5, RAMSizeGB = 128, RAMType = "DDR5", VideoCardType = "GTX 3080TI", VideoCardSizeGB = 24, StorageType = "SSD", StorageSizeGB = 2000 }, OperationSystem = new OperationSystem() { Id = 666666, Name = "Windows", Foundation = "2014", Ceo = "Elemér", SizeGB = 15, PriceHuf = 500 }, PriceHuf = 500 } };

            m2.Setup(x => x.ListAllAsLaptop()).Returns(lista);
            m2.Object.Insert(new Laptop() { Id = 666666, LaptopBrand = new LaptopBrand() { Id = 666666, Name = "Asus", Country = "Tajvan", Ceo = "Elemér", Url = "ezegyweboldal.com" }, Config = new Config() { Id = 666666, DisplaySizeCol = 15.6, DisplayType = "FullHD", ProcessorType = "I9-9900K", ProcessorSpeedGhz = 5, RAMSizeGB = 128, RAMType = "DDR5", VideoCardType = "GTX 3080TI", VideoCardSizeGB = 24, StorageType = "SSD", StorageSizeGB = 2000 }, OperationSystem = new OperationSystem() { Id = 666666, Name = "Windows", Foundation = "2014", Ceo = "Elemér", SizeGB = 15, PriceHuf = 500 }, PriceHuf = 500 });
            m2.Verify(mock => mock.Insert(new Laptop() { Id = 666666, LaptopBrand = new LaptopBrand() { Id = 666666, Name = "Asus", Country = "Tajvan", Ceo = "Elemér", Url = "ezegyweboldal.com" }, Config = new Config() { Id = 666666, DisplaySizeCol = 15.6, DisplayType = "FullHD", ProcessorType = "I9-9900K", ProcessorSpeedGhz = 5, RAMSizeGB = 128, RAMType = "DDR5", VideoCardType = "GTX 3080TI", VideoCardSizeGB = 24, StorageType = "SSD", StorageSizeGB = 2000 }, OperationSystem = new OperationSystem() { Id = 666666, Name = "Windows", Foundation = "2014", Ceo = "Elemér", SizeGB = 15, PriceHuf = 500 }, PriceHuf = 500 }), Times.Never());
        }

        /// <summary>
        /// This method checks if the given values from 1-10 are inside our fake list.
        /// </summary>
        /// <param name="value">The ID of the Brand.</param>
        [Test]
        [Sequential]
        public void BrandExistenceCheck([Values(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)] int value)
        {
            BrandLogic m = Mock.Of<BrandLogic>();
            Mock<BrandLogic> m2 = Mock.Get(m);
            List<LaptopBrand> lista = new List<LaptopBrand>();
            for (int i = 1; i < 11; i++)
            {
                lista.Add(new LaptopBrand() { Id = i, Name = "Asus", Country = "Tajvan", Ceo = "Elemér", Url = "ezegyweboldal.com" });
            }

            m2.Setup(x => x.ListAllAsBrand()).Returns(lista);

            Assert.That(m2.Object.CheckIsBrandExsists(value), Is.True);
        }

        /// <summary>
        /// This method checks if the given values from 1-10 are inside our fake list.
        /// </summary>
        /// <param name="value">The ID of the OS.</param>
        [Test]
        [Sequential]
        public void OSExistenceCheck([Values(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)] int value)
        {
            OSLogic m = Mock.Of<OSLogic>();
            Mock<OSLogic> m2 = Mock.Get(m);
            List<OperationSystem> lista = new List<OperationSystem>();
            for (int i = 1; i < 11; i++)
            {
                lista.Add(new OperationSystem() { Id = 666666, Name = "Windows", Foundation = "2014", Ceo = "Elemér", SizeGB = 15, PriceHuf = 500 });
            }

            m2.Setup(x => x.ListAllAsOS()).Returns(lista);

            Assert.That(m2.Object.CheckIsOSExsists(value), Is.True);
        }

        /// <summary>
        /// This method checks if the given values from 1-10 are inside our fake list.
        /// </summary>
        /// <param name="value">The ID of the Config.</param>
        [Test]
        [Sequential]
        public void ConfigExistenceCheck([Values(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)] int value)
        {
            ConfigLogic m = Mock.Of<ConfigLogic>();
            Mock<ConfigLogic> m2 = Mock.Get(m);
            List<Config> lista = new List<Config>();
            for (int i = 1; i < 11; i++)
            {
                lista.Add(new Config() { Id = 666666, DisplaySizeCol = 15.6, DisplayType = "FullHD", ProcessorType = "I9-9900K", ProcessorSpeedGhz = 5, RAMSizeGB = 128, RAMType = "DDR5", VideoCardType = "GTX 3080TI", VideoCardSizeGB = 24, StorageType = "SSD", StorageSizeGB = 2000 });
            }

            m2.Setup(x => x.ListAllAsConfig()).Returns(lista);

            Assert.That(m2.Object.CheckIsConfigExsists(value), Is.True);
        }

        /// <summary>
        /// This method checks if the given values from 1-10 are inside our fake list.
        /// </summary>
        /// <param name="value">The ID of the Laptop.</param>
        [Test]
        [Sequential]
        public void LaptopExistenceCheck([Values(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)] int value)
        {
            LaptopLogic m = Mock.Of<LaptopLogic>();
            Mock<LaptopLogic> m2 = Mock.Get(m);
            List<Laptop> lista = new List<Laptop>();
            for (int i = 1; i < 11; i++)
            {
                lista.Add(new Laptop() { Id = 666666, LaptopBrand = new LaptopBrand() { Id = 666666, Name = "Asus", Country = "Tajvan", Ceo = "Elemér", Url = "ezegyweboldal.com" }, Config = new Config() { Id = 666666, DisplaySizeCol = 15.6, DisplayType = "FullHD", ProcessorType = "I9-9900K", ProcessorSpeedGhz = 5, RAMSizeGB = 128, RAMType = "DDR5", VideoCardType = "GTX 3080TI", VideoCardSizeGB = 24, StorageType = "SSD", StorageSizeGB = 2000 }, OperationSystem = new OperationSystem() { Id = 666666, Name = "Windows", Foundation = "2014", Ceo = "Elemér", SizeGB = 15, PriceHuf = 500 }, PriceHuf = 500 });
            }

            m2.Setup(x => x.ListAllAsLaptop()).Returns(lista);

            Assert.That(m2.Object.CheckIsLaptopExsists(value), Is.True);
        }
    }
}
