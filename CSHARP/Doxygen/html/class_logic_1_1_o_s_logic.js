var class_logic_1_1_o_s_logic =
[
    [ "OSLogic", "class_logic_1_1_o_s_logic.html#add92e6ddca748253de0a91b5fb6177fc", null ],
    [ "CheckIsOSExsists", "class_logic_1_1_o_s_logic.html#aed31970e6d3ecfc50081e55515de304a", null ],
    [ "GetOSById", "class_logic_1_1_o_s_logic.html#ad7db684f948a10ccfd23352b19dc5f57", null ],
    [ "Insert", "class_logic_1_1_o_s_logic.html#a4ed5ef106b60eb5e80785c08a2391742", null ],
    [ "ListAll", "class_logic_1_1_o_s_logic.html#a0a60e64f35cf144c08a290f85d9c885a", null ],
    [ "ListAllAsOS", "class_logic_1_1_o_s_logic.html#ae97684949185cc5e21455383cf1f6268", null ],
    [ "ListAllFreeOS", "class_logic_1_1_o_s_logic.html#a76c0aef08d86aeec403b9a6caf9c7ff3", null ],
    [ "Remove", "class_logic_1_1_o_s_logic.html#a3a40daf4c9600a3b2368bb19da6cb9da", null ],
    [ "Update", "class_logic_1_1_o_s_logic.html#a2d3aed684e51cb59c4db7eeb4b8bf9ca", null ],
    [ "BaseRepository", "class_logic_1_1_o_s_logic.html#a80c9fc242ab4fcc4387e47f0d2ee93a5", null ]
];