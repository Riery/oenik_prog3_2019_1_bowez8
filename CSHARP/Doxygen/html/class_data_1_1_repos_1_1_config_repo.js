var class_data_1_1_repos_1_1_config_repo =
[
    [ "CheckIfConfigExists", "class_data_1_1_repos_1_1_config_repo.html#aab4505123ce47a0831fc8acafc5a9457", null ],
    [ "Delete", "class_data_1_1_repos_1_1_config_repo.html#a026aaa5505ed23f6d43ba845dd36ff12", null ],
    [ "I7AndAtLeast16GBRamConfigs", "class_data_1_1_repos_1_1_config_repo.html#a12adc5aedb042a5aba2d5c6ce091d8a0", null ],
    [ "Insert", "class_data_1_1_repos_1_1_config_repo.html#a35b56cc90869bdf05eaa8948fa474e89", null ],
    [ "ListAll", "class_data_1_1_repos_1_1_config_repo.html#ab85b85021ed8b8b0d5786ece5de933ba", null ],
    [ "ListTableContents", "class_data_1_1_repos_1_1_config_repo.html#a35bfa2690e9194815c445a65881606c7", null ],
    [ "Update", "class_data_1_1_repos_1_1_config_repo.html#abf8baf23e867f97ae1c7433b5430c8f6", null ]
];