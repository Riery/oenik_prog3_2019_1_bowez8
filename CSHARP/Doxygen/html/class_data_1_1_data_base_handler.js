var class_data_1_1_data_base_handler =
[
    [ "DataBaseHandler", "class_data_1_1_data_base_handler.html#a9185d1fe2f6ceb6990d89a79ad7658d0", null ],
    [ "AddNewBrand", "class_data_1_1_data_base_handler.html#aa350e91bba77d0137a54f73dffd12275", null ],
    [ "AddNewConfig", "class_data_1_1_data_base_handler.html#a7c0da2f1383b15fa753f1d0ac691e509", null ],
    [ "AddNewLaptop", "class_data_1_1_data_base_handler.html#aa77b2e18b2c1d379a45636f154b902f9", null ],
    [ "AddNewOS", "class_data_1_1_data_base_handler.html#ac88e38bbe794deea9506002188201c5d", null ],
    [ "DeleteBrand", "class_data_1_1_data_base_handler.html#a8406c5b6761574047f37c24050e89bd1", null ],
    [ "DeleteConfig", "class_data_1_1_data_base_handler.html#a61282631f7b6c33bb9efbc7a764ecfdc", null ],
    [ "DeleteLaptop", "class_data_1_1_data_base_handler.html#a493939dc39df4191185ff04a6e34f0b8", null ],
    [ "DeleteOS", "class_data_1_1_data_base_handler.html#a856d0048d185a8a4fc32b006253711d1", null ],
    [ "Dispose", "class_data_1_1_data_base_handler.html#a2cfcf9e5649b65d65d6be72afb7cf1dc", null ],
    [ "Dispose", "class_data_1_1_data_base_handler.html#a5be5fc9ff7697d52c60ae9a289b64d52", null ],
    [ "ListBrands", "class_data_1_1_data_base_handler.html#ae6d60e4f6d67d6bd412d595de089c4ef", null ],
    [ "ListConfigs", "class_data_1_1_data_base_handler.html#adcd95d5c752beb3f95916536e15c0c45", null ],
    [ "ListLaptops", "class_data_1_1_data_base_handler.html#aebfc1c296c842cdb1f00a4e0a3b1101d", null ],
    [ "ListOperationSystems", "class_data_1_1_data_base_handler.html#ad2b369a2c5155a18703a9659d8d745cf", null ],
    [ "SaveDB", "class_data_1_1_data_base_handler.html#aa637ee9b5a245da4fd9f8b71eca9da86", null ],
    [ "ShopDataBase", "class_data_1_1_data_base_handler.html#adcc9d914d7b0c9636189793a33ff2ef7", null ],
    [ "TablesNames", "class_data_1_1_data_base_handler.html#a8455f74a07032f000b53c274dafb4983", null ]
];