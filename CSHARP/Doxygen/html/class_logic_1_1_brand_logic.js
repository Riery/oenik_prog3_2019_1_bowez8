var class_logic_1_1_brand_logic =
[
    [ "BrandLogic", "class_logic_1_1_brand_logic.html#af96614b564400f03bdaea8235d9c4438", null ],
    [ "CheckIsBrandExsists", "class_logic_1_1_brand_logic.html#a0ca69874f2e920c53aabbd5ca0761d36", null ],
    [ "GetBrandById", "class_logic_1_1_brand_logic.html#a7da40e2187cfe0aad48fcf1ba5c574c2", null ],
    [ "Insert", "class_logic_1_1_brand_logic.html#a2834db999ea3ac90790099a6e1da3098", null ],
    [ "ListAll", "class_logic_1_1_brand_logic.html#af6b0e91cc7a5de8dd0bb6d92a594b78a", null ],
    [ "ListAllAsBrand", "class_logic_1_1_brand_logic.html#a43d2005ee9e92cb0d994d9ce888beb98", null ],
    [ "Remove", "class_logic_1_1_brand_logic.html#a9e556a58f15343a89e052fc03e0db93d", null ],
    [ "Update", "class_logic_1_1_brand_logic.html#aaa68a05cc22656026d857a3b40780d96", null ],
    [ "BaseRepository", "class_logic_1_1_brand_logic.html#a55ef8af940d33c0eb60e60966a2c244c", null ]
];