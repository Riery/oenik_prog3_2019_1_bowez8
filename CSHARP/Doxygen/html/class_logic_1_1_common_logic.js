var class_logic_1_1_common_logic =
[
    [ "AddBrand", "class_logic_1_1_common_logic.html#a0b8415887929cd71c5f43f0f6b8ecfa3", null ],
    [ "AddConfig", "class_logic_1_1_common_logic.html#af0967f25ac127b52b0c6286b1917de87", null ],
    [ "AddLaptop", "class_logic_1_1_common_logic.html#a6248708318958449cf9d66c779b3bedd", null ],
    [ "AddOS", "class_logic_1_1_common_logic.html#accc738bac1a3bc11bf39f6a5224940cb", null ],
    [ "ConsoleWrite", "class_logic_1_1_common_logic.html#ad46dfc8e16a435bac1e20cf3b954fb8f", null ],
    [ "RemoveBrand", "class_logic_1_1_common_logic.html#a493f636edee12f561e795770b8b38f8e", null ],
    [ "RemoveConfig", "class_logic_1_1_common_logic.html#a8496349e05a058b0ff723c73ee0f2488", null ],
    [ "RemoveLaptop", "class_logic_1_1_common_logic.html#a4518aebc0bdeefa1388981f86b3cacb1", null ],
    [ "RemoveOS", "class_logic_1_1_common_logic.html#a24f9e506938e1402d2fe0b786ee2f3a4", null ],
    [ "SubMenu", "class_logic_1_1_common_logic.html#a613d04e21d509ae39efebdbc6eea8548", null ],
    [ "UpdateBrand", "class_logic_1_1_common_logic.html#ad7644a80e3a170ad522ecf30a45b90e0", null ],
    [ "UpdateConfig", "class_logic_1_1_common_logic.html#acc3b884dbfcdd40b4f035c6d9c590a3b", null ],
    [ "UpdateLaptop", "class_logic_1_1_common_logic.html#a7ec9ff2199b0f812ca57a1a6afc37d6b", null ],
    [ "UpdateOS", "class_logic_1_1_common_logic.html#a207b02b5ded2395ce97afaf7f66dcbd0", null ]
];