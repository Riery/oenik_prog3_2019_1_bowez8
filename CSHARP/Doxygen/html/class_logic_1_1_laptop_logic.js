var class_logic_1_1_laptop_logic =
[
    [ "LaptopLogic", "class_logic_1_1_laptop_logic.html#a098b819b8ce9893e1cd41028fb8be4dc", null ],
    [ "CheckIsLaptopExsists", "class_logic_1_1_laptop_logic.html#a69ef05714a2e06fd50f346eabc9659e5", null ],
    [ "GetLaptopById", "class_logic_1_1_laptop_logic.html#a1104315fc1a3dcf217020c3e5f224010", null ],
    [ "Insert", "class_logic_1_1_laptop_logic.html#a0c598abccff0918ec75c4462b93817da", null ],
    [ "ListAll", "class_logic_1_1_laptop_logic.html#a00982ff46b1e0b6b6c267d2851c1faa9", null ],
    [ "ListAllAsLaptop", "class_logic_1_1_laptop_logic.html#a8a4b08493e89718c003e2defe3690e6d", null ],
    [ "Remove", "class_logic_1_1_laptop_logic.html#a2a45260e2c8116afa764521b16fb91ad", null ],
    [ "Update", "class_logic_1_1_laptop_logic.html#a3c6a332e863c6aa938edfae3fe3909ae", null ]
];