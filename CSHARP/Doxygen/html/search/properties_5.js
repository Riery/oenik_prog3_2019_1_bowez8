var searchData=
[
  ['laptopbrand',['LaptopBrand',['../class_data_1_1_laptop.html#ab7b28773d84e339ad6fc6c87b309055f',1,'Data::Laptop']]],
  ['laptopbrand_5fid',['LaptopBrand_Id',['../class_data_1_1_laptop.html#ae03467300321c809caef5ce82fced3c3',1,'Data::Laptop']]],
  ['laptopbrands',['LaptopBrands',['../class_data_1_1_shop_d_b_entities.html#a94908befb94e60e772494242ca279e51',1,'Data::ShopDBEntities']]],
  ['laptoprepository',['LaptopRepository',['../class_data_1_1_repos_1_1_base_repository.html#ac54f7188aa06436c3f1baba1c24af940',1,'Data::Repos::BaseRepository']]],
  ['laptops',['Laptops',['../class_data_1_1_config.html#a8bdd3586028ae9db8a25871d9f6cf705',1,'Data.Config.Laptops()'],['../class_data_1_1_laptop_brand.html#ae2f573ca9adafbc4aa65ed58678a905f',1,'Data.LaptopBrand.Laptops()'],['../class_data_1_1_operation_system.html#a589a1ed2b853f7deb262c7b15e009d0f',1,'Data.OperationSystem.Laptops()'],['../class_data_1_1_shop_d_b_entities.html#a2d67186cf58ea07f5382f94195a45ace',1,'Data.ShopDBEntities.Laptops()']]]
];
