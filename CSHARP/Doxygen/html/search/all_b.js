var searchData=
[
  ['ramsizegb',['RAMSizeGB',['../class_data_1_1_config.html#a5095cd14d936f18a131583aa43304e27',1,'Data::Config']]],
  ['ramtype',['RAMType',['../class_data_1_1_config.html#a76a5b8113b5316f7a2211251ebe0fb59',1,'Data::Config']]],
  ['remove',['Remove',['../class_logic_1_1_brand_logic.html#a9e556a58f15343a89e052fc03e0db93d',1,'Logic.BrandLogic.Remove()'],['../class_logic_1_1_config_logic.html#a8b159774524e5eee07773c2fd0568afc',1,'Logic.ConfigLogic.Remove()'],['../class_logic_1_1_laptop_logic.html#a2a45260e2c8116afa764521b16fb91ad',1,'Logic.LaptopLogic.Remove()'],['../class_logic_1_1_o_s_logic.html#a3a40daf4c9600a3b2368bb19da6cb9da',1,'Logic.OSLogic.Remove()']]],
  ['removebrand',['RemoveBrand',['../class_logic_1_1_common_logic.html#a493f636edee12f561e795770b8b38f8e',1,'Logic::CommonLogic']]],
  ['removeconfig',['RemoveConfig',['../class_logic_1_1_common_logic.html#a8496349e05a058b0ff723c73ee0f2488',1,'Logic::CommonLogic']]],
  ['removelaptop',['RemoveLaptop',['../class_logic_1_1_common_logic.html#a4518aebc0bdeefa1388981f86b3cacb1',1,'Logic::CommonLogic']]],
  ['removeos',['RemoveOS',['../class_logic_1_1_common_logic.html#a24f9e506938e1402d2fe0b786ee2f3a4',1,'Logic::CommonLogic']]]
];
