var searchData=
[
  ['baserepository',['BaseRepository',['../class_data_1_1_repos_1_1_base_repository.html',1,'Data.Repos.BaseRepository'],['../class_logic_1_1_brand_logic.html#a55ef8af940d33c0eb60e60966a2c244c',1,'Logic.BrandLogic.BaseRepository()'],['../class_logic_1_1_config_logic.html#a0b8e8daa627159eb8b768143da6bc985',1,'Logic.ConfigLogic.BaseRepository()'],['../class_logic_1_1_o_s_logic.html#a80c9fc242ab4fcc4387e47f0d2ee93a5',1,'Logic.OSLogic.BaseRepository()'],['../class_data_1_1_repos_1_1_base_repository.html#a89f53a0d2d340f0b53c00df2351a6dcb',1,'Data.Repos.BaseRepository.BaseRepository()']]],
  ['brandexistencecheck',['BrandExistenceCheck',['../class_test_1_1_logic_test.html#a7382171276bb57dbeef0a1575af32cba',1,'Test::LogicTest']]],
  ['brandlogic',['BrandLogic',['../class_logic_1_1_brand_logic.html',1,'Logic.BrandLogic'],['../class_logic_1_1_brand_logic.html#af96614b564400f03bdaea8235d9c4438',1,'Logic.BrandLogic.BrandLogic()']]],
  ['brandrepo',['BrandRepo',['../class_data_1_1_repos_1_1_brand_repo.html',1,'Data::Repos']]],
  ['brandrepository',['BrandRepository',['../class_data_1_1_repos_1_1_base_repository.html#a0a20440342f2948b204c5d1040c1390a',1,'Data::Repos::BaseRepository']]]
];
