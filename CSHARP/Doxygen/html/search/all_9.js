var searchData=
[
  ['onmodelcreating',['OnModelCreating',['../class_data_1_1_shop_d_b_entities.html#a669ccd34b34f0b495660eb4422466472',1,'Data::ShopDBEntities']]],
  ['operationsystem',['OperationSystem',['../class_data_1_1_operation_system.html',1,'Data.OperationSystem'],['../class_data_1_1_laptop.html#a9c16060e3daf7a26c2d5e17859d2d3a7',1,'Data.Laptop.OperationSystem()'],['../class_data_1_1_operation_system.html#aeb982150c0c645c35e4f0c61de592114',1,'Data.OperationSystem.OperationSystem()']]],
  ['operationsystem_5fid',['OperationSystem_Id',['../class_data_1_1_laptop.html#af8f23b2db37caf37042538896f6211b9',1,'Data::Laptop']]],
  ['operationsystems',['OperationSystems',['../class_data_1_1_shop_d_b_entities.html#a7babcee321ac62cded9b83599ba278da',1,'Data::ShopDBEntities']]],
  ['osexistencecheck',['OSExistenceCheck',['../class_test_1_1_logic_test.html#ada15e40e60705c95cf8ee4b0421daaf8',1,'Test::LogicTest']]],
  ['oslogic',['OSLogic',['../class_logic_1_1_o_s_logic.html',1,'Logic.OSLogic'],['../class_logic_1_1_o_s_logic.html#add92e6ddca748253de0a91b5fb6177fc',1,'Logic.OSLogic.OSLogic()']]],
  ['osrepo',['OSRepo',['../class_data_1_1_repos_1_1_o_s_repo.html',1,'Data::Repos']]],
  ['osrepository',['OSRepository',['../class_data_1_1_repos_1_1_base_repository.html#a1d1aeb100d5d4340b40b7e98d20e55f5',1,'Data::Repos::BaseRepository']]]
];
