var searchData=
[
  ['savedb',['SaveDB',['../class_data_1_1_data_base_handler.html#aa637ee9b5a245da4fd9f8b71eca9da86',1,'Data::DataBaseHandler']]],
  ['shopdatabase',['ShopDataBase',['../class_data_1_1_data_base_handler.html#adcc9d914d7b0c9636189793a33ff2ef7',1,'Data::DataBaseHandler']]],
  ['shopdbentities',['ShopDBEntities',['../class_data_1_1_shop_d_b_entities.html',1,'Data.ShopDBEntities'],['../class_data_1_1_shop_d_b_entities.html#adca6963bd87d2b6e12dd4e87488ed63b',1,'Data.ShopDBEntities.ShopDBEntities()']]],
  ['singleinsertbrandcheck',['SingleInsertBrandCheck',['../class_test_1_1_logic_test.html#a43076981e47735fef802b23c1232c4e2',1,'Test::LogicTest']]],
  ['singleinsertconfigcheck',['SingleInsertConfigCheck',['../class_test_1_1_logic_test.html#adae16f59e25c641e1b4f7e2fe3d9d0de',1,'Test::LogicTest']]],
  ['singleinsertlaptopcheck',['SingleInsertLaptopCheck',['../class_test_1_1_logic_test.html#a1e102d1903e25b78400ece15e3421cfa',1,'Test::LogicTest']]],
  ['singleinsertoperationsystemcheck',['SingleInsertOperationSystemCheck',['../class_test_1_1_logic_test.html#ae22ab372ab3eae5568dc05aa835f8e9b',1,'Test::LogicTest']]],
  ['sizegb',['SizeGB',['../class_data_1_1_operation_system.html#a9f5d8c707ccdb54a9a9d66a5d2dd2b30',1,'Data::OperationSystem']]],
  ['storagesizegb',['StorageSizeGB',['../class_data_1_1_config.html#aa13e830f3f5f073a29ad9ddf2bc27cdd',1,'Data::Config']]],
  ['storagetype',['StorageType',['../class_data_1_1_config.html#aeaccc36996bda1f0dcdc996f7e4abb98',1,'Data::Config']]],
  ['submenu',['SubMenu',['../class_logic_1_1_common_logic.html#a613d04e21d509ae39efebdbc6eea8548',1,'Logic::CommonLogic']]]
];
