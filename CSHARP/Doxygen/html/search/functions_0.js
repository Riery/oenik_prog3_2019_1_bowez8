var searchData=
[
  ['addbrand',['AddBrand',['../class_logic_1_1_common_logic.html#a0b8415887929cd71c5f43f0f6b8ecfa3',1,'Logic::CommonLogic']]],
  ['addconfig',['AddConfig',['../class_logic_1_1_common_logic.html#af0967f25ac127b52b0c6286b1917de87',1,'Logic::CommonLogic']]],
  ['addlaptop',['AddLaptop',['../class_logic_1_1_common_logic.html#a6248708318958449cf9d66c779b3bedd',1,'Logic::CommonLogic']]],
  ['addnewbrand',['AddNewBrand',['../class_data_1_1_data_base_handler.html#aa350e91bba77d0137a54f73dffd12275',1,'Data::DataBaseHandler']]],
  ['addnewconfig',['AddNewConfig',['../class_data_1_1_data_base_handler.html#a7c0da2f1383b15fa753f1d0ac691e509',1,'Data::DataBaseHandler']]],
  ['addnewlaptop',['AddNewLaptop',['../class_data_1_1_data_base_handler.html#aa77b2e18b2c1d379a45636f154b902f9',1,'Data::DataBaseHandler']]],
  ['addnewos',['AddNewOS',['../class_data_1_1_data_base_handler.html#ac88e38bbe794deea9506002188201c5d',1,'Data::DataBaseHandler']]],
  ['addos',['AddOS',['../class_logic_1_1_common_logic.html#accc738bac1a3bc11bf39f6a5224940cb',1,'Logic::CommonLogic']]]
];
