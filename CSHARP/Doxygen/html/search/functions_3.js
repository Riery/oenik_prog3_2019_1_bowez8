var searchData=
[
  ['databasehandler',['DataBaseHandler',['../class_data_1_1_data_base_handler.html#a9185d1fe2f6ceb6990d89a79ad7658d0',1,'Data::DataBaseHandler']]],
  ['delete',['Delete',['../class_data_1_1_repos_1_1_brand_repo.html#a2b76d5868882d71960a9f3680987f6bb',1,'Data.Repos.BrandRepo.Delete()'],['../class_data_1_1_repos_1_1_config_repo.html#a026aaa5505ed23f6d43ba845dd36ff12',1,'Data.Repos.ConfigRepo.Delete()'],['../interface_data_1_1_interfaces_1_1_i_repository.html#afbc6bb4fcbebdc8da713025316f9d284',1,'Data.Interfaces.IRepository.Delete()'],['../class_data_1_1_repos_1_1_laptop_repo.html#a407d88d79151cda2b326e23b8c9c083e',1,'Data.Repos.LaptopRepo.Delete()'],['../class_data_1_1_repos_1_1_o_s_repo.html#a70518cea1458d63e2804f37a199251e4',1,'Data.Repos.OSRepo.Delete()']]],
  ['deletebrand',['DeleteBrand',['../class_data_1_1_data_base_handler.html#a8406c5b6761574047f37c24050e89bd1',1,'Data::DataBaseHandler']]],
  ['deleteconfig',['DeleteConfig',['../class_data_1_1_data_base_handler.html#a61282631f7b6c33bb9efbc7a764ecfdc',1,'Data::DataBaseHandler']]],
  ['deletelaptop',['DeleteLaptop',['../class_data_1_1_data_base_handler.html#a493939dc39df4191185ff04a6e34f0b8',1,'Data::DataBaseHandler']]],
  ['deleteos',['DeleteOS',['../class_data_1_1_data_base_handler.html#a856d0048d185a8a4fc32b006253711d1',1,'Data::DataBaseHandler']]],
  ['dispose',['Dispose',['../class_data_1_1_data_base_handler.html#a2cfcf9e5649b65d65d6be72afb7cf1dc',1,'Data.DataBaseHandler.Dispose()'],['../class_data_1_1_data_base_handler.html#a5be5fc9ff7697d52c60ae9a289b64d52',1,'Data.DataBaseHandler.Dispose(bool disposing)']]]
];
