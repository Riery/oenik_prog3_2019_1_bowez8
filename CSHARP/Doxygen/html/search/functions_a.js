var searchData=
[
  ['update',['Update',['../class_data_1_1_repos_1_1_brand_repo.html#afd43690a01c65fb62f056c50f070b59e',1,'Data.Repos.BrandRepo.Update()'],['../class_data_1_1_repos_1_1_config_repo.html#abf8baf23e867f97ae1c7433b5430c8f6',1,'Data.Repos.ConfigRepo.Update()'],['../class_data_1_1_repos_1_1_laptop_repo.html#a872b83e6217d93a7f5c85128d99ea213',1,'Data.Repos.LaptopRepo.Update()'],['../class_data_1_1_repos_1_1_o_s_repo.html#a167f577f34faa9dc8a2911454d4f0dcf',1,'Data.Repos.OSRepo.Update()'],['../class_logic_1_1_brand_logic.html#aaa68a05cc22656026d857a3b40780d96',1,'Logic.BrandLogic.Update()'],['../class_logic_1_1_config_logic.html#a0ecbe3910dfc0fc35c51917e02fb590e',1,'Logic.ConfigLogic.Update()'],['../class_logic_1_1_laptop_logic.html#a3c6a332e863c6aa938edfae3fe3909ae',1,'Logic.LaptopLogic.Update()'],['../class_logic_1_1_o_s_logic.html#a2d3aed684e51cb59c4db7eeb4b8bf9ca',1,'Logic.OSLogic.Update()']]],
  ['updatebrand',['UpdateBrand',['../class_logic_1_1_common_logic.html#ad7644a80e3a170ad522ecf30a45b90e0',1,'Logic::CommonLogic']]],
  ['updateconfig',['UpdateConfig',['../class_logic_1_1_common_logic.html#acc3b884dbfcdd40b4f035c6d9c590a3b',1,'Logic::CommonLogic']]],
  ['updatelaptop',['UpdateLaptop',['../class_logic_1_1_common_logic.html#a7ec9ff2199b0f812ca57a1a6afc37d6b',1,'Logic::CommonLogic']]],
  ['updateos',['UpdateOS',['../class_logic_1_1_common_logic.html#a207b02b5ded2395ce97afaf7f66dcbd0',1,'Logic::CommonLogic']]]
];
