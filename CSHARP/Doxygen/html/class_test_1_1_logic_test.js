var class_test_1_1_logic_test =
[
    [ "BrandExistenceCheck", "class_test_1_1_logic_test.html#a7382171276bb57dbeef0a1575af32cba", null ],
    [ "ConfigExistenceCheck", "class_test_1_1_logic_test.html#a721149d06e7fdf3039e1e8c9aa664c23", null ],
    [ "LaptopExistenceCheck", "class_test_1_1_logic_test.html#a07629d8a2d28aa750d05b031f5cefdb2", null ],
    [ "OSExistenceCheck", "class_test_1_1_logic_test.html#ada15e40e60705c95cf8ee4b0421daaf8", null ],
    [ "SingleInsertBrandCheck", "class_test_1_1_logic_test.html#a43076981e47735fef802b23c1232c4e2", null ],
    [ "SingleInsertConfigCheck", "class_test_1_1_logic_test.html#adae16f59e25c641e1b4f7e2fe3d9d0de", null ],
    [ "SingleInsertLaptopCheck", "class_test_1_1_logic_test.html#a1e102d1903e25b78400ece15e3421cfa", null ],
    [ "SingleInsertOperationSystemCheck", "class_test_1_1_logic_test.html#ae22ab372ab3eae5568dc05aa835f8e9b", null ]
];