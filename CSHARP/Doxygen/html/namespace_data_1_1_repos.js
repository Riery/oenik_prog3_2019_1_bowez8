var namespace_data_1_1_repos =
[
    [ "BaseRepository", "class_data_1_1_repos_1_1_base_repository.html", "class_data_1_1_repos_1_1_base_repository" ],
    [ "BrandRepo", "class_data_1_1_repos_1_1_brand_repo.html", "class_data_1_1_repos_1_1_brand_repo" ],
    [ "ConfigRepo", "class_data_1_1_repos_1_1_config_repo.html", "class_data_1_1_repos_1_1_config_repo" ],
    [ "LaptopRepo", "class_data_1_1_repos_1_1_laptop_repo.html", "class_data_1_1_repos_1_1_laptop_repo" ],
    [ "OSRepo", "class_data_1_1_repos_1_1_o_s_repo.html", "class_data_1_1_repos_1_1_o_s_repo" ]
];