var namespace_logic =
[
    [ "BrandLogic", "class_logic_1_1_brand_logic.html", "class_logic_1_1_brand_logic" ],
    [ "CommonLogic", "class_logic_1_1_common_logic.html", "class_logic_1_1_common_logic" ],
    [ "ConfigLogic", "class_logic_1_1_config_logic.html", "class_logic_1_1_config_logic" ],
    [ "LaptopLogic", "class_logic_1_1_laptop_logic.html", "class_logic_1_1_laptop_logic" ],
    [ "OSLogic", "class_logic_1_1_o_s_logic.html", "class_logic_1_1_o_s_logic" ]
];