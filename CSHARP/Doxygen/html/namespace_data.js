var namespace_data =
[
    [ "Interfaces", "namespace_data_1_1_interfaces.html", "namespace_data_1_1_interfaces" ],
    [ "Repos", "namespace_data_1_1_repos.html", "namespace_data_1_1_repos" ],
    [ "Config", "class_data_1_1_config.html", "class_data_1_1_config" ],
    [ "DataBaseHandler", "class_data_1_1_data_base_handler.html", "class_data_1_1_data_base_handler" ],
    [ "Laptop", "class_data_1_1_laptop.html", "class_data_1_1_laptop" ],
    [ "LaptopBrand", "class_data_1_1_laptop_brand.html", "class_data_1_1_laptop_brand" ],
    [ "OperationSystem", "class_data_1_1_operation_system.html", "class_data_1_1_operation_system" ],
    [ "ShopDBEntities", "class_data_1_1_shop_d_b_entities.html", "class_data_1_1_shop_d_b_entities" ]
];