var class_data_1_1_config =
[
    [ "Config", "class_data_1_1_config.html#acb0dcc682476804c411cccc7d874f24b", null ],
    [ "DisplaySizeCol", "class_data_1_1_config.html#a3e8d25c9e32f0bd9e3a3ba39c5f0816e", null ],
    [ "DisplayType", "class_data_1_1_config.html#af471457be2a0c39ed3f4455adca413c9", null ],
    [ "Id", "class_data_1_1_config.html#a6da0515f4f1e43040755d6e28c97dcff", null ],
    [ "Laptops", "class_data_1_1_config.html#a8bdd3586028ae9db8a25871d9f6cf705", null ],
    [ "ProcessorSpeedGhz", "class_data_1_1_config.html#a053ff0578a0e1b4046ca48a68b63185e", null ],
    [ "ProcessorType", "class_data_1_1_config.html#a385264ded6363f40b17af6cfc1618b5e", null ],
    [ "RAMSizeGB", "class_data_1_1_config.html#a5095cd14d936f18a131583aa43304e27", null ],
    [ "RAMType", "class_data_1_1_config.html#a76a5b8113b5316f7a2211251ebe0fb59", null ],
    [ "StorageSizeGB", "class_data_1_1_config.html#aa13e830f3f5f073a29ad9ddf2bc27cdd", null ],
    [ "StorageType", "class_data_1_1_config.html#aeaccc36996bda1f0dcdc996f7e4abb98", null ],
    [ "VideoCardSizeGB", "class_data_1_1_config.html#a3022f79aee563e66a3b5e13d12e33d5b", null ],
    [ "VideoCardType", "class_data_1_1_config.html#ab5a71c8341169d1dc288726e34bedfe9", null ]
];