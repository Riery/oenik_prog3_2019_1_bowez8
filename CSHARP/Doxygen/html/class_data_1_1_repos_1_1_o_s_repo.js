var class_data_1_1_repos_1_1_o_s_repo =
[
    [ "CheckIfOSExists", "class_data_1_1_repos_1_1_o_s_repo.html#a0725b78bc73d3eb2200d7afc7c9fc739", null ],
    [ "Delete", "class_data_1_1_repos_1_1_o_s_repo.html#a70518cea1458d63e2804f37a199251e4", null ],
    [ "Insert", "class_data_1_1_repos_1_1_o_s_repo.html#aab71c558b164a519b39711aff75c9af2", null ],
    [ "ListAll", "class_data_1_1_repos_1_1_o_s_repo.html#a5a61d1fa59e4f784521a171950b8b109", null ],
    [ "ListAllFreeOS", "class_data_1_1_repos_1_1_o_s_repo.html#adff7b6cc8c34cd92b7ccdbcbf5ad2f1d", null ],
    [ "ListTableContents", "class_data_1_1_repos_1_1_o_s_repo.html#ade61d75e3ef20f18b52312d63e2ec5d2", null ],
    [ "Update", "class_data_1_1_repos_1_1_o_s_repo.html#a167f577f34faa9dc8a2911454d4f0dcf", null ]
];