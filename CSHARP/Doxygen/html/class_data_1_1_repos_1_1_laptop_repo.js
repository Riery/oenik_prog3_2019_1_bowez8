var class_data_1_1_repos_1_1_laptop_repo =
[
    [ "CheckIfLaptopExists", "class_data_1_1_repos_1_1_laptop_repo.html#a842f79e1ec0418d4676785da7dc9ca25", null ],
    [ "Delete", "class_data_1_1_repos_1_1_laptop_repo.html#a407d88d79151cda2b326e23b8c9c083e", null ],
    [ "Insert", "class_data_1_1_repos_1_1_laptop_repo.html#ac2fa3a52d704b84258485a9c5f21bd57", null ],
    [ "ListAll", "class_data_1_1_repos_1_1_laptop_repo.html#a2cdc8f9131a68f0c6b6a643c71ac3b91", null ],
    [ "ListAllLaptopsBelow200KHuf", "class_data_1_1_repos_1_1_laptop_repo.html#a16baace51bd6df39e88caada86a9a2fc", null ],
    [ "ListTableContents", "class_data_1_1_repos_1_1_laptop_repo.html#a1201b5a7800b29c57f85d6f894c3844b", null ],
    [ "Update", "class_data_1_1_repos_1_1_laptop_repo.html#a872b83e6217d93a7f5c85128d99ea213", null ]
];