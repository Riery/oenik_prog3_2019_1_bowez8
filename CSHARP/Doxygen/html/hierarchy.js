var hierarchy =
[
    [ "Data.Repos.BaseRepository", "class_data_1_1_repos_1_1_base_repository.html", null ],
    [ "Logic.BrandLogic", "class_logic_1_1_brand_logic.html", null ],
    [ "Logic.CommonLogic", "class_logic_1_1_common_logic.html", null ],
    [ "Logic.ConfigLogic", "class_logic_1_1_config_logic.html", null ],
    [ "DbContext", null, [
      [ "Data.ShopDBEntities", "class_data_1_1_shop_d_b_entities.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "Data.DataBaseHandler", "class_data_1_1_data_base_handler.html", null ]
    ] ],
    [ "Data.Interfaces.IEntity", "interface_data_1_1_interfaces_1_1_i_entity.html", [
      [ "Data.Config", "class_data_1_1_config.html", null ],
      [ "Data.Laptop", "class_data_1_1_laptop.html", null ],
      [ "Data.LaptopBrand", "class_data_1_1_laptop_brand.html", null ],
      [ "Data.OperationSystem", "class_data_1_1_operation_system.html", null ]
    ] ],
    [ "Data.Interfaces.IRepository< T >", "interface_data_1_1_interfaces_1_1_i_repository.html", null ],
    [ "Data.Interfaces.IRepository< Config >", "interface_data_1_1_interfaces_1_1_i_repository.html", [
      [ "Data.Repos.ConfigRepo", "class_data_1_1_repos_1_1_config_repo.html", null ]
    ] ],
    [ "Data.Interfaces.IRepository< Laptop >", "interface_data_1_1_interfaces_1_1_i_repository.html", [
      [ "Data.Repos.LaptopRepo", "class_data_1_1_repos_1_1_laptop_repo.html", null ]
    ] ],
    [ "Data.Interfaces.IRepository< LaptopBrand >", "interface_data_1_1_interfaces_1_1_i_repository.html", [
      [ "Data.Repos.BrandRepo", "class_data_1_1_repos_1_1_brand_repo.html", null ]
    ] ],
    [ "Data.Interfaces.IRepository< OperationSystem >", "interface_data_1_1_interfaces_1_1_i_repository.html", [
      [ "Data.Repos.OSRepo", "class_data_1_1_repos_1_1_o_s_repo.html", null ]
    ] ],
    [ "Logic.LaptopLogic", "class_logic_1_1_laptop_logic.html", null ],
    [ "Test.LogicTest", "class_test_1_1_logic_test.html", null ],
    [ "Logic.OSLogic", "class_logic_1_1_o_s_logic.html", null ],
    [ "Test.Tests", "class_test_1_1_tests.html", null ]
];