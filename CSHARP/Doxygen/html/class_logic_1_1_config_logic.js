var class_logic_1_1_config_logic =
[
    [ "ConfigLogic", "class_logic_1_1_config_logic.html#ad773b02bc221857a5fb6002dff65a2db", null ],
    [ "CheckIsConfigExsists", "class_logic_1_1_config_logic.html#a88328d3bbf213c68d0aafe009b4587be", null ],
    [ "GetConfigById", "class_logic_1_1_config_logic.html#a85ac6e4ce843f777bf34243ffd3012d2", null ],
    [ "Insert", "class_logic_1_1_config_logic.html#aa0214555a4d784cc903dba6167e9b7a9", null ],
    [ "ListAll", "class_logic_1_1_config_logic.html#a03647b45b7d079af89a1321db5ddd7cd", null ],
    [ "ListAllAsConfig", "class_logic_1_1_config_logic.html#ab8c8fd29cd60659149cc9e4909ca64e8", null ],
    [ "Remove", "class_logic_1_1_config_logic.html#a8b159774524e5eee07773c2fd0568afc", null ],
    [ "Update", "class_logic_1_1_config_logic.html#a0ecbe3910dfc0fc35c51917e02fb590e", null ],
    [ "BaseRepository", "class_logic_1_1_config_logic.html#a0b8e8daa627159eb8b768143da6bc985", null ]
];