var class_data_1_1_laptop =
[
    [ "Config", "class_data_1_1_laptop.html#a930883335d1b25edb172e8e78942b41a", null ],
    [ "Config_Id", "class_data_1_1_laptop.html#afff74ad33d563a26dabe6cb0bb4fbf1e", null ],
    [ "Id", "class_data_1_1_laptop.html#ad09c23093152d7a91d67b8ed3af576ce", null ],
    [ "LaptopBrand", "class_data_1_1_laptop.html#ab7b28773d84e339ad6fc6c87b309055f", null ],
    [ "LaptopBrand_Id", "class_data_1_1_laptop.html#ae03467300321c809caef5ce82fced3c3", null ],
    [ "OperationSystem", "class_data_1_1_laptop.html#a9c16060e3daf7a26c2d5e17859d2d3a7", null ],
    [ "OperationSystem_Id", "class_data_1_1_laptop.html#af8f23b2db37caf37042538896f6211b9", null ],
    [ "PriceHuf", "class_data_1_1_laptop.html#aec913f5931ae415f615a22a44c554044", null ]
];