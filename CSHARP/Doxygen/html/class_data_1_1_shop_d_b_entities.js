var class_data_1_1_shop_d_b_entities =
[
    [ "ShopDBEntities", "class_data_1_1_shop_d_b_entities.html#adca6963bd87d2b6e12dd4e87488ed63b", null ],
    [ "OnModelCreating", "class_data_1_1_shop_d_b_entities.html#a669ccd34b34f0b495660eb4422466472", null ],
    [ "Configs", "class_data_1_1_shop_d_b_entities.html#ab1de675d888bd9c4cd49411240bee1f1", null ],
    [ "LaptopBrands", "class_data_1_1_shop_d_b_entities.html#a94908befb94e60e772494242ca279e51", null ],
    [ "Laptops", "class_data_1_1_shop_d_b_entities.html#a2d67186cf58ea07f5382f94195a45ace", null ],
    [ "OperationSystems", "class_data_1_1_shop_d_b_entities.html#a7babcee321ac62cded9b83599ba278da", null ]
];