var class_data_1_1_repos_1_1_brand_repo =
[
    [ "CheckIfBrandExists", "class_data_1_1_repos_1_1_brand_repo.html#ac0562882d284257bcda1ecccbec96365", null ],
    [ "Delete", "class_data_1_1_repos_1_1_brand_repo.html#a2b76d5868882d71960a9f3680987f6bb", null ],
    [ "GetTheBrandByCountry", "class_data_1_1_repos_1_1_brand_repo.html#a58b1b28988b4b40b451771a00bdb56ab", null ],
    [ "Insert", "class_data_1_1_repos_1_1_brand_repo.html#af0531e9fb4ce7e3652e4b7b857f54f30", null ],
    [ "ListAll", "class_data_1_1_repos_1_1_brand_repo.html#a6b3b29c7009d260572249cd0e1177507", null ],
    [ "ListTableContents", "class_data_1_1_repos_1_1_brand_repo.html#a024d496574b0f273a26d126c8ced788c", null ],
    [ "Update", "class_data_1_1_repos_1_1_brand_repo.html#afd43690a01c65fb62f056c50f070b59e", null ]
];