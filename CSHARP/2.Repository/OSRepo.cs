﻿// <copyright file="OSRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Repos
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data.Interfaces;

    /// <summary>
    /// Specific repository which represents OS functions.
    /// </summary>
    public class OSRepo : IRepository<OperationSystem>
    {
        /// <summary>
        /// List all.
        /// </summary>
        /// <returns>List.</returns>
        public IQueryable<OperationSystem> ListAll()
        {
            return DataBaseHandler.Instance.ListOperationSystems().AsQueryable<OperationSystem>();
        }

        /// <summary>
        /// List table contents.
        /// </summary>
        /// <returns>Table content.</returns>
        public StringBuilder ListTableContents()
        {
            List<OperationSystem> oses = this.ListAll().ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var os in oses)
            {
                builder.AppendLine(" - " + os.Id + ". " + os.Name + ", " + os.Foundation +
                ", " + os.Ceo + ", " + os.SizeGB + "GB, " + os.PriceHuf + "Ft");
            }

            return builder;
        }

        /// <summary>
        /// Sends data to the database handler to insert a new OS.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="ceo">Ceo.</param>
        /// <param name="foundation">Foundation.</param>
        /// <param name="sizeGB">Size gb.</param>
        /// <param name="priceHuf">Price huf.</param>
        /// <returns>Success or not.</returns>
        public bool Insert(string name, string ceo, string foundation, int sizeGB, int priceHuf)
        {
            var newOs = new OperationSystem()
            {
                Name = name,
                Ceo = ceo,
                Foundation = foundation,
                SizeGB = sizeGB,
                PriceHuf = priceHuf,
            };
            DataBaseHandler.Instance.AddNewOS(newOs);
            DataBaseHandler.Instance.SaveDB();
            return true;
        }

        /// <summary>
        /// Sends data to the database handler to update an OS.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="name">Name.</param>
        /// <param name="ceo">Ceo.</param>
        /// <param name="foundation">Foundation.</param>
        /// <param name="sizeGB">Size gb.</param>
        /// <param name="priceHuf">Price huf.</param>
        /// <returns>Upadte.</returns>
        public bool Update(int id, string name, string ceo, string foundation, int sizeGB, int priceHuf)
        {
            OperationSystem os = this.ListAll().First(o => o.Id == id);
            if (os != null)
            {
                os.Name = name;
                os.Ceo = ceo;
                os.Foundation = foundation;
                os.SizeGB = sizeGB;
                os.PriceHuf = priceHuf;
                DataBaseHandler.Instance.SaveDB();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Success or not.</returns>
        public bool Delete(int id)
        {
            OperationSystem os = this.ListAll().First(b => b.Id == id);
            if (os != null)
            {
                DataBaseHandler.Instance.DeleteOS(os);
                DataBaseHandler.Instance.SaveDB();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check if os exsists.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>success or not.</returns>
        public bool CheckIfOSExists(int id)
        {
            return this.ListAll().Any(entity => entity.Id != id);
        }

        /// <summary>
        /// List all free os.
        /// </summary>
        /// <returns>String builder.</returns>
        public StringBuilder ListAllFreeOS()
        {
            List<OperationSystem> oses = this.ListAll().Where(entity => entity.PriceHuf == 0).ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var os in oses)
            {
                builder.AppendLine(" - " + os.Id + ". " + os.Name + ", " + os.Foundation +
                ", " + os.Ceo + ", " + os.SizeGB + "GB, ");
            }

            return builder;
        }
    }
}
