﻿// <copyright file="LaptopRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Repos
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data.Interfaces;

    /// <summary>
    /// Specific repository which represents laptop functions.
    /// </summary>
    public class LaptopRepo : IRepository<Laptop>
    {
        /// <summary>
        /// Lists all.
        /// </summary>
        /// <returns>List.</returns>
        public IQueryable<Laptop> ListAll()
        {
            return DataBaseHandler.Instance.ListLaptops().AsQueryable<Laptop>();
        }

        /// <summary>
        /// List table contents.
        /// </summary>
        /// <returns>table content.</returns>
        public StringBuilder ListTableContents()
        {
            List<Laptop> laptops = this.ListAll().ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var laptop in laptops)
            {
                builder.AppendLine(" - " + laptop.Id + ". " + laptop.LaptopBrand.Name + ", " + laptop.Config.DisplaySizeCol + "-" + laptop.Config.DisplayType + ", " +
                laptop.Config.ProcessorType + "-" + laptop.Config.ProcessorSpeedGhz + "Ghz, " + laptop.Config.RAMSizeGB +
                "GB-" + laptop.Config.RAMType + ", " + laptop.Config.VideoCardType + "-" + laptop.Config.VideoCardSizeGB +
                "GB, " + laptop.Config.StorageType + "-" + laptop.Config.StorageSizeGB + "GB, " + laptop.OperationSystem.Name +
                ", Price: " + laptop.PriceHuf + " Ft");
            }

            return builder;
        }

        /// <summary>
        /// Sends data to the database handler to insert a new laptop.
        /// </summary>
        /// <param name="brand">Brand.</param>
        /// <param name="config">Config.</param>
        /// <param name="os">OS.</param>
        /// <param name="price">Price.</param>
        /// <returns>Insert.</returns>
        public bool Insert(LaptopBrand brand, Config config, OperationSystem os, int? price)
        {
            var newLaptop = new Laptop()
            {
                LaptopBrand_Id = brand.Id,
                Config_Id = config.Id,
                OperationSystem_Id = os.Id,
                PriceHuf = price,
            };
            DataBaseHandler.Instance.AddNewLaptop(newLaptop);
            DataBaseHandler.Instance.SaveDB();
            return true;
        }

        /// <summary>
        /// Sends data to the database handler to update a laptop.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="brand">Brand.</param>
        /// <param name="config">Config.</param>
        /// <param name="os">OS.</param>
        /// <param name="price">Price.</param>
        /// <returns>Success or not.</returns>
        public bool Update(int id, LaptopBrand brand, Config config, OperationSystem os, int? price)
        {
            Laptop laptop = this.ListAll().First(l => l.Id == id);
            if (laptop != null)
            {
                laptop.LaptopBrand_Id = brand.Id;
                laptop.Config_Id = config.Id;
                laptop.OperationSystem_Id = os.Id;
                laptop.PriceHuf = price;
                DataBaseHandler.Instance.SaveDB();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Deleted or not.</returns>
        public bool Delete(int id)
        {
            Laptop laptop = this.ListAll().First(b => b.Id == id);
            if (laptop != null)
            {
                DataBaseHandler.Instance.DeleteLaptop(laptop);
                DataBaseHandler.Instance.SaveDB();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check if laptop exists.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Success or not.</returns>
        public bool CheckIfLaptopExists(int id)
        {
            return this.ListAll().Any(entity => entity.Id != id);
        }

        /// <summary>
        /// List all laptops below 200k huf.
        /// </summary>
        /// <returns>String builder.</returns>
        public StringBuilder ListAllLaptopsBelow200KHuf()
        {
            List<Laptop> laptops = this.ListAll().Where(entity => entity.PriceHuf < 200000).ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var laptop in laptops)
            {
                builder.AppendLine(" - " + laptop.Id + ". " + laptop.LaptopBrand.Name + ", " + laptop.Config.DisplaySizeCol + "-" + laptop.Config.DisplayType + ", " +
                laptop.Config.ProcessorType + "-" + laptop.Config.ProcessorSpeedGhz + "Ghz, " + laptop.Config.RAMSizeGB +
                "GB-" + laptop.Config.RAMType + ", " + laptop.Config.VideoCardType + "-" + laptop.Config.VideoCardSizeGB +
                "GB, " + laptop.Config.StorageType + "-" + laptop.Config.StorageSizeGB + "GB, " + laptop.OperationSystem.Name +
                ", Price: " + laptop.PriceHuf + " Ft");
            }

            return builder;
        }
    }
}
