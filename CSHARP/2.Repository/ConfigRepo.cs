﻿// <copyright file="ConfigRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Repos
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data.Interfaces;

    /// <summary>
    /// Specific repository which represents config functions.
    /// </summary>
    public class ConfigRepo : IRepository<Config>
    {
        /// <summary>
        /// List all.
        /// </summary>
        /// <returns>List.</returns>
        public IQueryable<Config> ListAll()
        {
            return DataBaseHandler.Instance.ListConfigs().AsQueryable<Config>();
        }

        /// <summary>
        /// List table contents.
        /// </summary>
        /// <returns>Table content.</returns>
        public StringBuilder ListTableContents()
        {
            List<Config> configs = this.ListAll().ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var config in configs)
            {
                builder.AppendLine(" - " + config.Id + ". " + config.DisplaySizeCol + "-" + config.DisplayType + ", " +
                config.ProcessorType + "-" + config.ProcessorSpeedGhz + "Ghz, " + config.RAMSizeGB +
                "GB-" + config.RAMType + ", " + config.VideoCardType + "-" + config.VideoCardSizeGB +
                "GB, " + config.StorageType + "-" + config.StorageSizeGB + "GB");
            }

            return builder;
        }

        /// <summary>
        /// Sends data to the database handler to insert a new config.
        /// </summary>
        /// <param name="displayType">Display type.</param>
        /// <param name="displaySize">Display size.</param>
        /// <param name="processorType">Processor type.</param>
        /// <param name="processorSpeedGhz">Processor speed.</param>
        /// <param name="ramType">Ram type.</param>
        /// <param name="ramSizeGB">Ram size.</param>
        /// <param name="videoCardType">Video card type.</param>
        /// <param name="videoCardSizeGB">Video card size gb.</param>
        /// <param name="storageType">storage type.</param>
        /// <param name="storageSizeGB">storage size.</param>
        /// <returns>Success or not.</returns>
        public bool Insert(string displayType, double displaySize, string processorType, double processorSpeedGhz, string ramType, int ramSizeGB, string videoCardType, int videoCardSizeGB, string storageType, int storageSizeGB)
        {
            var cfg = new Config()
            {
                DisplayType = displayType,
                DisplaySizeCol = displaySize,
                ProcessorType = processorType,
                ProcessorSpeedGhz = processorSpeedGhz,
                RAMType = ramType,
                RAMSizeGB = ramSizeGB,
                VideoCardType = videoCardType,
                VideoCardSizeGB = videoCardSizeGB,
                StorageType = storageType,
                StorageSizeGB = storageSizeGB,
            };
            DataBaseHandler.Instance.AddNewConfig(cfg);
            DataBaseHandler.Instance.SaveDB();
            return true;
        }

        /// <summary>
        /// Sends data to the database handler to update a config.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="displayType">Display type.</param>
        /// <param name="displaySize">Display size.</param>
        /// <param name="processorType">Processor Type.</param>
        /// <param name="processorSpeedGhz">Processor speed ghz.</param>
        /// <param name="ramType">Ram type.</param>
        /// <param name="ramSizeGB">Ram size gb.</param>
        /// <param name="videoCardType">Video card type.</param>
        /// <param name="videoCardSizeGB">Video card size gb.</param>
        /// <param name="storageType">Storage type.</param>
        /// <param name="storageSizeGB">Storage size gb.</param>
        /// <returns>Success or not.</returns>
        public bool Update(int id, string displayType, double displaySize, string processorType, double processorSpeedGhz, string ramType, int ramSizeGB, string videoCardType, int videoCardSizeGB, string storageType, int storageSizeGB)
        {
            Config config = this.ListAll().First(c => c.Id == id);
            if (config != null)
            {
                config.DisplayType = displayType;
                config.DisplaySizeCol = displaySize;
                config.ProcessorType = processorType;
                config.ProcessorSpeedGhz = processorSpeedGhz;
                config.RAMType = ramType;
                config.RAMSizeGB = ramSizeGB;
                config.VideoCardType = videoCardType;
                config.VideoCardSizeGB = videoCardSizeGB;
                config.StorageType = storageType;
                config.StorageSizeGB = storageSizeGB;
                DataBaseHandler.Instance.SaveDB();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Succes or not.</returns>
        public bool Delete(int id)
        {
            Config cfg = this.ListAll().First(b => b.Id == id);
            if (cfg != null)
            {
                DataBaseHandler.Instance.DeleteConfig(cfg);
                DataBaseHandler.Instance.SaveDB();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check if config exsists.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Success or not.</returns>
        public bool CheckIfConfigExists(int id)
        {
            return this.ListAll().Any(entity => entity.Id != id);
        }

        /// <summary>
        /// i7 and at least 16gb ram.
        /// </summary>
        /// <returns>String builder.</returns>
        public StringBuilder I7AndAtLeast16GBRamConfigs()
        {
            List<Config> configs = this.ListAll().Where(entity => entity.ProcessorType.Contains("i7") && entity.RAMSizeGB >= 16).ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var config in configs)
            {
                builder.AppendLine(" - " + config.Id + ". " + config.DisplaySizeCol + "-" + config.DisplayType + ", " +
                config.ProcessorType + "-" + config.ProcessorSpeedGhz + "Ghz, " + config.RAMSizeGB +
                "GB-" + config.RAMType + ", " + config.VideoCardType + "-" + config.VideoCardSizeGB +
                "GB, " + config.StorageType + "-" + config.StorageSizeGB + "GB");
            }

            return builder;
        }
    }
}
