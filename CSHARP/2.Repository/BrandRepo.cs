﻿// <copyright file="BrandRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Repos
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data.Interfaces;

    /// <summary>
    /// Specific repository which represents laptop brand functions.
    /// </summary>
    public class BrandRepo : IRepository<LaptopBrand>
    {
        /// <summary>
        /// List all.
        /// </summary>
        /// <returns>List.</returns>
        public IQueryable<LaptopBrand> ListAll()
        {
            return DataBaseHandler.Instance.ListBrands().AsQueryable<LaptopBrand>();
        }

        /// <summary>
        /// List Table contents.
        /// </summary>
        /// <returns>String builder.</returns>
        public StringBuilder ListTableContents()
        {
            List<LaptopBrand> brands = this.ListAll().ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var brand in brands)
            {
                builder.AppendLine(" - " + brand.Id + ". " + brand.Name + ", " + brand.Country + ", " + brand.Ceo + ", " + brand.Url);
            }

            return builder;
        }

        /// <summary>
        /// Sends data to the database handler to insert a new brand.
        /// </summary>
        /// <param name="ceo">Coeo.</param>
        /// <param name="country">Country.</param>
        /// <param name="name">Name.</param>
        /// <param name="url">Url.</param>
        /// <returns>Success or not.</returns>
        public bool Insert(string ceo, string country, string name, string url)
        {
            var newBrand = new LaptopBrand()
            {
                Ceo = ceo,
                Country = country,
                Name = name,
                Url = url,
            };
            DataBaseHandler.Instance.AddNewBrand(newBrand);
            DataBaseHandler.Instance.SaveDB();
            return true;
        }

        /// <summary>
        /// Sends data to the database handler to update a brand.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="ceo">Ceo.</param>
        /// <param name="country">Country.</param>
        /// <param name="name">Name.</param>
        /// <param name="url">Url.</param>
        /// <returns>Succes or not.</returns>
        public bool Update(int id, string ceo, string country, string name, string url)
        {
            LaptopBrand brand = this.ListAll().First(b => b.Id == id);
            if (brand != null)
            {
                brand.Ceo = ceo;
                brand.Country = country;
                brand.Name = name;
                brand.Url = url;
                DataBaseHandler.Instance.SaveDB();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Success or not.</returns>
        public bool Delete(int id)
        {
            LaptopBrand brand = this.ListAll().First(b => b.Id == id);
            if (brand != null)
            {
                DataBaseHandler.Instance.DeleteBrand(brand);
                DataBaseHandler.Instance.SaveDB();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check if brand exists.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Success or not.</returns>
        public bool CheckIfBrandExists(int id)
        {
            return this.ListAll().Any(entity => entity.Id != id);
        }

        /// <summary>
        /// Get the brand by country.
        /// </summary>
        /// <param name="country">Country.</param>
        /// <returns>List.</returns>
        public LaptopBrand GetTheBrandByCountry(string country)
        {
            return this.ListAll().FirstOrDefault(entity => entity.Country.Contains(country));
        }
    }
}
