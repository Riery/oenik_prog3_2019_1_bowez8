﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Interfaces
{
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Repository Interface.
    /// </summary>
    /// <typeparam name="T">TEntity.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// List table contents.
        /// </summary>
        /// <returns>table content.</returns>
        StringBuilder ListTableContents();

        /// <summary>
        /// List all.
        /// </summary>
        /// <returns>List.</returns>
        IQueryable<T> ListAll();

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Success or not.</returns>
        bool Delete(int id);
    }
}
