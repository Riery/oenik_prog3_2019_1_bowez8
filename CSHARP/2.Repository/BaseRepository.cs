﻿// <copyright file="BaseRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Repos
{
    using System.Collections.Generic;

    /// <summary>
    /// This Repository supports getting the specific repositories.
    /// </summary>
    public class BaseRepository
    {
        private readonly BrandRepo brandRepo;
        private readonly ConfigRepo configRepo;
        private readonly LaptopRepo laptopRepo;
        private readonly OSRepo osRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository"/> class.
        /// Base repo constructor.
        /// </summary>
        public BaseRepository()
        {
            this.brandRepo = new BrandRepo();
            this.configRepo = new ConfigRepo();
            this.laptopRepo = new LaptopRepo();
            this.osRepo = new OSRepo();
        }

        /// <summary>
        /// Gets brand repo.
        /// </summary>
        public virtual BrandRepo BrandRepository
        {
            get { return this.brandRepo; }
        }

        /// <summary>
        /// Gets config repo getter.
        /// </summary>
        public virtual ConfigRepo ConfigRepository
        {
            get { return this.configRepo; }
        }

        /// <summary>
        /// Gets laptop repo getter.
        /// </summary>
        public virtual LaptopRepo LaptopRepository
        {
            get { return this.laptopRepo; }
        }

        /// <summary>
        /// Gets os repo getter.
        /// </summary>
        public virtual OSRepo OSRepository
        {
            get { return this.osRepo; }
        }

        /// <summary>
        /// List all table names.
        /// </summary>
        /// <returns>Table names.</returns>
        public List<string> ListAllTableNames()
        {
            return DataBaseHandler.Instance.TablesNames;
        }
    }
}
